package com.StudentRegistration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.fieldvalidation.DateValidation;
import com.fieldvalidation.MobileEmailValidation;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
public class StudentDetailsImpl {
	 static List<StudentDetails> list = new ArrayList<StudentDetails>();
	 
	 /**
	 * This method Used to Add new Student Record
	 *
	 * @throws IOException 
	 */
	public void enRollNewStudent() throws IOException {
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		list = getStudentList();
		
		StudentDetails student = new StudentDetails();
		MobileEmailValidation validate = new MobileEmailValidation();
		
		long student_id = StudentDetailsImpl.getNextStudentId();
		student.setStudent_id(student_id);
		
		System.out.println("Enter the First Name");
		String firstName = sc.next();
		student.setFirstname(firstName);
		
		System.out.println("Enter the Last Name");
		String lastName = sc.next();
		student.setLastname(lastName);
		
		System.out.println("Enter the emailid");
		String email_id = sc.next();
		boolean flag = true;
		do {
			if(validate.isValidEmailId(email_id)) {
				student.setEmail_id(email_id);
				flag = false;
				break;
			} else {
				System.out.println("Please Enter valid Email Address");
				email_id = sc.next();
			}
		}while(flag);
		
		
		System.out.println("Enter the Mobile Number");
		String mobile_no= sc.next();
		boolean flag1 = true;
		do {
			if(validate.isValidMobileNumber(mobile_no)) {
				student.setMobile_no(mobile_no);
				flag1 = false;
				break;
			} else {
				System.out.println("Please Enter Correct Mobile Number");
				mobile_no= sc.next();
			}
		}while(flag1);
		
		
		System.out.println("Enter the parent no");
		String parent_no= sc.next();
		boolean flag2 = true;
		do {
			if(validate.isValidMobileNumber(parent_no)) {
				student.setParent_no(parent_no);
				flag2 = false;
				break;
			} else {
				System.out.println("Please Enter Correct Mobile Number");
				parent_no= sc.next();
			}
		}while(flag2);
		
		
		System.out.println("Enter the address");
		String address= sc.next();
		student.setAddress(address);
		
		System.out.println("Enter the branch(mpc/bpc)");
		String branch= sc.next();
		while(!(branch.equalsIgnoreCase("mpc"))||!(branch.equalsIgnoreCase("bpc")))
		{
			System.out.println("please enter valid branch(MPC/BPC)");
			 branch= sc.next();
		}
		student.setBranch(branch);
	
		
		System.out.println("Enter the date of birth (DD/MM/YYYY)");
        DateValidation datevalid=new DateValidation();
        int choice=1;
        String date_of_birth=datevalid.isThisDateValid(choice);
        student.setDate_of_birth(date_of_birth);
        
		System.out.println("Enter the date of joining");
		String date_of_joining=datevalid.isThisDateValid(choice);
		student.setDate_of_joining(date_of_joining);
		
		System.out.println("New Student Added Successfully.");
		
		list.add(student);
		saveStudentRecord(list);
	
	}
	
	/**
	 * This method return List Object Stored in file
	 * 
	 * @throws FileNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	public static List<StudentDetails> getStudentList() throws FileNotFoundException {
		FileInputStream fi = new FileInputStream(new File("ExamTrackingDataFiles\\Std_Data.ser"));
		
		List<StudentDetails> list2 = new ArrayList<StudentDetails>();
		
		try{
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<StudentDetails>) oi.readObject();
		}catch(Exception e){
		   System.out.println();
		}
		
		return list2;
	}
	
	/**
	 * This method recently added or updated record in file
	 * 
	 * @throws IOException 
	 */
	public static void saveStudentRecord(List<StudentDetails> list) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File("ExamTrackingDataFiles\\Std_Data.ser"));
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			 obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		obj.writeObject(list);
		
	}
	
	/**
	 * This method print complete Student list from file
	 * 
	 * @throws FileNotFoundException 
	 */
	public static void printStudentAllRecords() throws FileNotFoundException {
		list= getStudentList();
		if(list.size() == 0) {
			System.out.println("Student List is Empty");
		} else {
			Iterator<StudentDetails> it = list.iterator();
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	        System.out.printf("%5s %28s %28s %28s %20s %20s %30s %15s %20s %20s", "Student ID", "First Name", "Last Name", "Email-Id", "Mobile","Parent Mobile" ,"Address" ,"Branch","DOB","DOJ");
	        System.out.println();
	        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	        while (it.hasNext()) {
	        	StudentDetails st1 = (StudentDetails)it.next();
	            System.out.format("%5s %30s %30s %30s %20s %20s %30s %15s %20s %20s",st1.getStudent_id() ,st1.getFirstname() ,st1.getLastname(), st1.getEmail_id(), st1.getMobile_no(),st1.getParent_no(),st1.getAddress(),st1.getBranch(),st1.getDate_of_birth(),st1.getDate_of_joining());
	            System.out.println();
	        }
	        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		}
		
	}
	/**
	 * This method gives Auto Generated Student id to insert record
	 * 
	 * @throws FileNotFoundException 
	 */
	public static long getNextStudentId() {
		long std_id = 101;
		try {
			list = getStudentList();
			if(list.size() == 0) {
				std_id = 101;
			} else {
				StudentDetails student = new StudentDetails();
				student = list.get(list.size() - 1);
				std_id = (student.getStudent_id() + 1);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return std_id;   
	}



public static void printEligibleRecords() throws FileNotFoundException {
	list = getStudentList();
	@SuppressWarnings("resource")
	Scanner sc=new Scanner(System.in);
	System.out.println("enter the id to view the data");
	long id = sc.nextLong();
	if (list.size() == 0) {
		System.out.println("Student List is Empty");
	} else {
		Iterator<StudentDetails> it = list.iterator();
		while (it.hasNext()) {
			StudentDetails st1 = (StudentDetails) it.next();
			if (id == st1.getStudent_id()) {
				System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		        System.out.printf("%5s %28s %28s %28s %20s %20s %30s %15s %20s %20s", "Student ID", "First Name", "Last Name", "Email-Id", "Mobile","Parent Mobile" ,"Address" ,"Branch","DOB","DOJ");
		        System.out.println();
		        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			
		        System.out.format("%5s %30s %30s %30s %20s %20s %30s %15s %20s %20s",st1.getStudent_id() ,st1.getFirstname() ,st1.getLastname(), st1.getEmail_id(), st1.getMobile_no(),st1.getParent_no(),st1.getAddress(),st1.getBranch(),st1.getDate_of_birth(),st1.getDate_of_joining());
	            System.out.println();}

		}
        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

	
	}
}
}

