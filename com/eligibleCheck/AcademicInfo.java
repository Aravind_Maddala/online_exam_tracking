package com.eligibleCheck;
import java.io.Serializable;
/**
 * 
 * @author BATCH-'C'
 *
 *This is class is usded to check the eligiblity of student Academic information
 */
public class AcademicInfo implements Serializable{
		/** This method is used to ddeclare the student id attedance
		  *  and fee to check the eligiblity of the student
	      */
	private static final long serialVersionUID = 1L;
		/**
		 *
		 * @param fee is variable to check student pay details
		 */
		// variables to declare the student id and attendance 
		private long student_id;
		private long attendance;
		private String fee;
		private String check;
		// getter setter methods for Academic info
		/**
		 * @return student_id
		 */
		public long getStudent_id() {
			return student_id;
		}
		public void setStudent_id(long student_id) {
			this.student_id = student_id;
		}
		/**
		 * @return attendance
		 */
		public long getAttendance() {
			return attendance;
		}
		public void setAttendance(long attendance) {
			this.attendance = attendance;
		}
		/**
		 * 
		 * @return fee
		 */
		public String getFee() {
			return fee;
		}
		public void setFee(String fee) {
			this.fee = fee;
		}
		/**
		 * 
		 * @return check
		 */
		public String getCheck() {
			return check;
		}
		public void setCheck(String check) {
			this.check = check;
		} 
	}

