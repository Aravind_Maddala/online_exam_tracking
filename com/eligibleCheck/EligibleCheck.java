package com.eligibleCheck;
//predefined packages
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.StudentRegistration.StudentDetails;

/** @author BATCH-'C'
 * This class is used to check Eligibility criteria for examination
 * according to priority of attendance vand fee
 *
 *
 */
public class EligibleCheck {
	// intiliazing scanner
	static Scanner sc = new Scanner(System.in);
	// creating a object for academic details of student
	AcademicInfo academicInfo = new AcademicInfo();
	// creating a list for Academicinfo
	static List<AcademicInfo> list = new ArrayList<AcademicInfo>();
	String check=null;

	/**
	 *  This method to view the eligible check for student
	 * @throws IOException
	 */
	public void setEligible() throws IOException {
		list = getAcademicList();
		System.out.println("enter the student id to set the eligibility criteria");
		long stud_id = Long.parseLong(sc.nextLine());
		boolean checkid = CheckId(stud_id);
		boolean checkid11=true;
		if (checkid) {
			Iterator<AcademicInfo> it = list.iterator();
		    while (it.hasNext()) {
			AcademicInfo st1 = (AcademicInfo) it.next();
			if(stud_id== st1.getStudent_id()) {
				checkid11=false;
				}
			}
			// to set the eligiblity check for the student 
			if(checkid11) {
			System.out.println("set the attendance for " + stud_id + "\n");
			System.out.println("enter the number of days present by student id");
			int attendance = Integer.parseInt(sc.nextLine());
			academicInfo.setAttendance(attendance);
			boolean setattendance = setAttendance(attendance);
			System.out.println("set the fee status for " + stud_id + "\n");
			String fee = sc.nextLine();
			academicInfo.setFee(fee);
			boolean setFee = setFee(fee);
			academicInfo.setStudent_id(stud_id);
			boolean check1=EligibleCheck.check(setattendance, setFee);
			if(check1) {
				check="yes";
			}
			else {
				check="no";
			}
			academicInfo.setCheck(check);
			list.add(academicInfo);
			setAcademicRecord(list);
			
			if (setattendance && setFee) {
				System.out.println("student is eligible for exam");
				list = getAcademicList();
			} else if (setattendance || !(setFee)) {
				System.out.println("student is not eligible for exam");

				list = getAcademicList();

				System.out.println("fee is pending");

				System.out.println("oops!!!Better luck next time");

			} else if (!(setattendance) || (setFee)) {

				System.out.println("student is not eligible for exam");

				list = getAcademicList();

				System.out.println("attendance is less than 50 your are not eligible for examination");

			}	

		}
			else {
				System.out.println("student status is already set");
			}
		}

		else {
			System.out.println("Id not found,please enter the correct student  id for signup");
			setEligible();
		}
	}

	/**
	 * method for fetching the student_id from StudentDetails
	 * @param std_id
	 * @return list
	 * @throws FileNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public static boolean CheckId(long std_id) throws FileNotFoundException {
		FileInputStream fi = new FileInputStream(new File("ExamTrackingDataFiles\\Std_Data.ser"));

		List<StudentDetails> list2 = new ArrayList<StudentDetails>(); // list for creating the StudentDetails

		try {
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<StudentDetails>) oi.readObject();
			@SuppressWarnings("rawtypes")
			Iterator itr = list2.iterator();
			while (itr.hasNext()) {
				StudentDetails std = (StudentDetails) itr.next();
				if (std.getStudent_id() == std_id) {
					return true;
				}
			}
		} catch (Exception e) {
			System.out.println();
		}
		return false;
	}

	/**
	 *  This method for checking attendance for eligibility of exam or not
	 * @param attendance
	 * @return
	 */
	public static boolean setAttendance(long attendance) {

		while (attendance < 0 || attendance > 150) {
			System.out.println("please enter the valid number of days present by student");
			attendance = Integer.parseInt(sc.nextLine());
		}
		double percentage = (attendance * 100.0) / 150;
		System.out.println("attendance:" + percentage);
		if (percentage > 50) {

			return true;
		}

		return false;

	}
    /**
     *  This method 
     * @param fee
     * @return
     */
	public static boolean setFee(String fee) { // method to check fee status of a student eligiblity of exam or no
		if (fee.equalsIgnoreCase("paid")) {
			return true;
		}
		return false;
	}
	public static boolean  check(boolean setattendance,boolean setFee) {
		if (setattendance && setFee) {
			return true;
		}
		return false;
	}

	// method for setting academic details into a new file
	public static void setAcademicRecord(List<AcademicInfo> list) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File("ExamTrackingDataFiles\\EligibleCheck.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
			e.printStackTrace();
		}

		obj.writeObject(list);

	}

	// method for getting StudentInfo list from file
	@SuppressWarnings("unchecked")
	public static List<AcademicInfo> getAcademicList() throws FileNotFoundException {
		FileInputStream fi = new FileInputStream(new File("ExamTrackingDataFiles\\EligibleCheck.txt"));

		List<AcademicInfo> list2 = new ArrayList<AcademicInfo>();

		try {
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<AcademicInfo>) oi.readObject();
		} catch (Exception e) {
			System.out.println();
		}

		return list2;
	}

	// method to print all records in the particular student Details
	public static void printEligibleAllRecords() throws FileNotFoundException {
		list = getAcademicList();
		if (list.size() == 0) {
			System.out.println("Student List is Empty");
		} else {
			
			System.out.println(
					"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			System.out.printf("%5s %28s %28s %25s", "Student ID", "Attendance percenatge", "fee","eligible check");
			System.out.println();
			System.out.println(
			
				"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			Iterator<AcademicInfo> it = list.iterator();
			while (it.hasNext()) {
				AcademicInfo st1 = (AcademicInfo) it.next();
				long percentage=(long) ((st1.getAttendance()*100)/150.0);
				System.out.format("%5s %30s %30s %25s ", st1.getStudent_id(), percentage, st1.getFee(),st1.getCheck());
				System.out.println();
			}
			System.out.println(
					"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		}
	}

	// method to print all student Details
	public static void printEligibleRecords() throws FileNotFoundException {
		list = getAcademicList();
		System.out.println("enter the student id to view the eligibility check");
		long id = sc.nextLong();
		if (list.size() == 0) {
			System.out.println("Student List is Empty");
		} else {
			Iterator<AcademicInfo> it = list.iterator();
			while (it.hasNext()) {
				AcademicInfo st1 = (AcademicInfo) it.next();
				if (id == st1.getStudent_id()) {
					System.out.println(
							"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
					System.out.printf("%5s %28s %28s ", "Student ID", "Attendance", "fee");
					System.out.println();
					System.out.println(
							"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
					System.out.format("%5s %30s %30s ", st1.getStudent_id(), st1.getAttendance(), st1.getFee());
				}

			}

			System.out.println(
					"\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		}
	}

}
