package com.admin.service;
/**
 * @author Batch C
 * This class use to get all students information from user.
 */
public class RegisterStudent {
	//creating data members to add new student in file 
	private String std_id;
	private String firstName;
	private String lastName;
	private String Address;
	private String mobile;
	private String email;
	private String gender;
	private String date_of_birth;
	private String date_of_joining;
	
	/***
	 *@return Student Id  
	 */
	public String getStd_id() {
		return std_id;
	}
	public void setStd_id(String std_id) {
		this.std_id = std_id;
	}
	/***
	 *@return firstName  
	 */
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/***
	 *@return Last Name  
	 */
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/***
	 *@return Address  
	 */
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	/***
	 *@return Mobile number of student  
	 */
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/***
	 *@return Email Address  
	 */
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	/***
	 *@return gender  
	 */
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	/***
	 *@return date of birth  
	 */
	public String getDate_of_birth() {
		return date_of_birth;
	}
	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}
	/***
	 *@return return date of joining   
	 */
	public String getDate_of_joining() {
		return date_of_joining;
	}
	public void setDate_of_joining(String date_of_joining) {
		this.date_of_joining = date_of_joining;
	}
}
