package com.admin.service;

import com.authentication.LoginUser;
/**
 * 
 * @author BATCH-'C'
 * This Is Main class For LOgin into the Users 
 *
 */
public class MainClass {
    /**
     * This method is used for login page for users to  Select the Admin/Student
     * @param args
     */
	public static void main(String[] args) {
		//creating object for login user class for logging in for users
		LoginUser loginUser=new LoginUser();
		
		//calling method for login 
		loginUser.loginUser();

	}

}
