package com.student.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.StudentRegistration.StudentDetails;
import com.StudentRegistration.StudentDetailsImpl;
import com.fieldvalidation.MobileEmailValidation;
/**
 * 
 * @author BATCH-'C'
 * this class is used to update student information
 */
public class UpdateInfo {
	// creating the list for the StudentDetails
	static List<StudentDetails> list = new ArrayList<StudentDetails>();
    /**
     * this method is used to check id for student 
     * @return
     * @throws IOException
     */
	public static boolean checkStudentForUpdate() throws IOException {
		Scanner sc = new Scanner(System.in);
		boolean flag = false;
		try {
			list = StudentDetailsImpl.getStudentList();
			Iterator<StudentDetails> it = list.iterator();
			System.out.println("Enter Student id to update");
			long id = sc.nextLong();
	        while (it.hasNext()) {
	        	StudentDetails st1 = (StudentDetails)it.next();
	        	if(st1.getStudent_id() == id) {
	        		getStudentForUpdate(st1 , list, sc);
	        		flag = true;
	        		break;
	        	}
	        }
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		}
		return flag;
	}
	
	/**
	 *  this method if student record found it will perform update operation on
	 * some of field like  emailid mobileno address
	 * @param 
	 */
	public static void getStudentForUpdate(StudentDetails student , List<StudentDetails> list, Scanner sc) {
		
		MobileEmailValidation validate = new MobileEmailValidation();
		System.out.println("select operation");
		System.out.println("1) Update mobile number");
		System.out.println("2) update email id");
		System.out.println("3) update address");
		int choice=0;
		try {
			choice=sc.nextInt();
			while(choice<=0||choice>3) {
				choice=sc.nextInt();
			}
		}
		catch(Exception e) {
			System.err.println("please enter integers only");
		}
		// if you choose option 1 you can update the mobile number
		if(choice==1) {
		System.out.println("Enter Updated mobile Number");
		String mobile_no= sc.next();
		boolean flag1 = true;
		do {
			if(validate.isValidMobileNumber(mobile_no)) {
				student.setMobile_no(mobile_no);
				flag1 = false;
				break;
			} else {
				System.out.println("Please Enter Correct Mobile Number");
				mobile_no= sc.next();
			}
		}while(flag1);
		}
		// if you choose option 2 you can update the emailid
		if(choice==2) {
		System.out.println("Enter Update Email Id");
		String email_id = sc.next();
		boolean flag = true;
		do {
			if(validate.isValidEmailId(email_id)) {
				student.setEmail_id(email_id);
				flag = false;
				break;
			} else {
				System.out.println("Please Enter valid Email Address");
				email_id = sc.next();
			}
		}while(flag);
		}
		// if you choose the option 3 you can update the address
		if (choice==3) {
		System.out.println("Enter New address");
		String address= sc.next();
		student.setAddress(address);
		}
 
		try {
			StudentDetailsImpl.saveStudentRecord(list);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		try {
			list = StudentDetailsImpl.getStudentList();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		System.out.println("Data Updated Successfully");
		
	}

}