package com.examination;

import java.io.Serializable;

public class SetExamTimeTable implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String date;
	private String subject;
	private String time;
	private String venue;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	@Override
	public String toString() {
		return "SetExamTimeTable [date=" + date + ", subject=" + subject + ", time=" + time + ", venue=" + venue + "]";
	}
}
