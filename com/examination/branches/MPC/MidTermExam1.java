package com.examination.branches.MPC;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.examination.SetExamTimeTable;
import com.fieldvalidation.DateValidation;
/**
 * 
 * @author BATCH-'C'
 * This class is used to update and display the timetable
 */
public class MidTermExam1 {
	// creating the  list for SetExamtimetable
	static List<SetExamTimeTable> list = new ArrayList<SetExamTimeTable>();
	/**
	 * This method is used to set the timetable
	 * @param filePath
	 * @throws IOException
	 */
	public static void SetMidTerm1TimeTable(String filePath) throws IOException {
		Scanner sc = new Scanner(System.in);	
		list = getExamTimeTableList(filePath);
		if(list.size() > 0) {
			System.out.println("Time Table is already set, do you want to re-set (yes/no)");
			String choice = sc.next();
			if(choice.equalsIgnoreCase("yes")) {
				list.clear();
				updateMidTerm1TimeTable(sc, filePath );
			} else {
				displayTimeTable(filePath , "First Term");
			}
		} else {
			updateMidTerm1TimeTable(sc, filePath);
		}
	}
	/**
	 * this method is used to update the timetable
	 * @param sc
	 * @param filePath
	 */
	public static void updateMidTerm1TimeTable(Scanner sc , String filePath) {
		ArrayList<SetExamTimeTable> midTerm1List = new ArrayList<SetExamTimeTable>();
		System.out.println("How many subject you want add");
		int subSize = sc.nextInt();
		int choice=2;
		for(int i = 0; i < subSize; i++) {
			SetExamTimeTable setMid1 = new SetExamTimeTable();
			System.out.println("Enter Date");
			DateValidation dateValid=new DateValidation();
			 String date=dateValid.isThisDateValid(choice);
			 setMid1.setDate(date);
			System.out.println("Enter Subject");
			String subject = sc.next();
			setMid1.setSubject(subject);
			
			System.out.println("Enter Time");
			String time = sc.next();
			setMid1.setTime(time);
			
			System.out.println("Enter Venue");
			String venue = sc.next();
			setMid1.setVenue(venue);
			
			midTerm1List.add(setMid1);
		}
		System.out.println("Added successfully");
		list.addAll(midTerm1List);
		try {
			saveExamTimeTableList(list,filePath);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	/**
	 * This method is used to get the listobjects  timetable from the file
	 * @param filePath
	 * @return
	 * @throws FileNotFoundException
	 */
	public static List<SetExamTimeTable> getExamTimeTableList(String filePath) throws FileNotFoundException {
		FileInputStream fi =null; 
		
		List<SetExamTimeTable> list2 = new ArrayList<SetExamTimeTable>();
		
		try{
			fi=new FileInputStream(new File(filePath));
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<SetExamTimeTable>) oi.readObject();
		}catch(Exception e){
		   System.out.println();
		}
		return list2;
	}
	/**
	 * This method is used to write the listobjects to the file
	 * @param list
	 * @param filePath
	 * @throws IOException
	 */
	public static void saveExamTimeTableList(List<SetExamTimeTable> list , String filePath) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File(filePath));
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			 obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		obj.writeObject(list);
	}
	/**
	 * This method is used to Display the student timetable list
	 * @param filePath
	 * @param typeOfExam
	 * @throws FileNotFoundException
	 */
	public static void displayTimeTable(String filePath , String typeOfExam) throws FileNotFoundException {
		list = getExamTimeTableList(filePath);
		if(list.size() == 0) {
			System.out.println("Time Table Not Upadte");
		} else {
			
			Iterator<SetExamTimeTable> it = list.iterator();
			System.out.println("Time Table for " +typeOfExam+" Exam");
			System.out.println("---------------------------------------------------------------------------------------------------------");
	        System.out.printf("%20s %20s %20s %20s ", "Date", "Time", "Subject", "Venue");
	        System.out.println();
	        System.out.println("---------------------------------------------------------------------------------------------------------");
	        while (it.hasNext()) {
	        	SetExamTimeTable setTime = (SetExamTimeTable)it.next();
	            System.out.format("%20s %20s %20s %20s",setTime.getDate(),setTime.getTime(),setTime.getSubject(),setTime.getVenue());
	            System.out.println();
	        }
	        System.out.println("---------------------------------------------------------------------------------------------------------");
		}
	}
}
