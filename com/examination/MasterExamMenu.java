package com.examination;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import com.admin.service.AdminMenu;
import com.eligibleCheck.EligibleCheck;
import com.examination.branches.MPC.MidTermExam1;

public class MasterExamMenu {
public static void getMasterMenuForExam() throws FileNotFoundException {
		
		String choiceToContinue = null;
		do {
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			//Displaying Menu to Admin
			System.out.println("==============================================");
			System.out.println("\nWelcome to Examination portal");
			System.out.println("==============================================");
			System.out.println("select operation");
			System.out.println("----------------------------------------------");
			System.out.println("1) Set Exam Time Table ");
			System.out.println("2) Eligibility check for examination");
			System.out.println("3) Set and View Marks of Students");
			System.out.println("4) View Time Table");
			System.out.println("5) Go Back To Admin Menu");
			System.out.println("----------------------------------------------");
			int choice=0;
			try {
				choice = sc.nextInt();
			}
			catch(Exception e)
			{
				System.out.println("enter the valid choice");
				getMasterMenuForExam();
			}
			
			
			switch(choice) {
				case 1: {
					//after selection type of exam this method set exam time table  
					ExamMenu.getExaminationMenu();
					break;
				}
				case 2: {
					try {
						System.out.println("==============================");
						System.out.println("Welcome to Eligibility check portal");
						System.out.println("==============================");
						System.out.println("select operation");
						System.out.println("-------------------------------");
						
						System.out.println("1)set the eligibility check of student");
						System.out.println("2)view the eligibility check of student");
						System.out.println("3)view the eligibility check of all student");
						int choice1=sc.nextInt();
	
						
						switch(choice1) 
						{
						case 1: EligibleCheck eligibleCheck=new EligibleCheck();
						eligibleCheck.setEligible();
						break;
						case 2:
						EligibleCheck.printEligibleRecords();
						break;
						case 3:  
						EligibleCheck.printEligibleAllRecords();
						break;
						
						default:
							System.out.println("you have enterted the wrong choice:");
							break;
						}
					} catch (IOException e) {
						
						e.printStackTrace();
					}
					
					break;
				}
				case 3: {
					SetMarksMenu.SetMarksSubMenu();
					break;
				}
				case 4: {
					try {
						MidTermExam1.displayTimeTable("ExamTrackingDataFiles\\MPCmid1timetable" , "First Term");
						MidTermExam1.displayTimeTable("ExamTrackingDataFiles\\MPCmid2timetable", "Second Term");
						MidTermExam1.displayTimeTable("ExamTrackingDataFiles\\MPCFinaltimetable", "Final");
						
					} catch (FileNotFoundException e) {
						
						e.printStackTrace();
					}
					break;
				}
				case 5:{
					AdminMenu menu = new AdminMenu();
					try {
						menu.adminFunctions();
					} catch (IOException e) {
				
						e.printStackTrace();
					}
					break;
				}
			}
			//here can choose option to continue
			System.out.println("Do You want to continue(yes/no)");
			choiceToContinue = sc.next(); 
		}while(choiceToContinue.equalsIgnoreCase("yes"));
	}
}
