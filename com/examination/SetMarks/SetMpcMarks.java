package com.examination.SetMarks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.StudentRegistration.StudentDetails;
import com.StudentRegistration.StudentDetailsImpl;
import com.eligibleCheck.AcademicInfo;
import com.eligibleCheck.EligibleCheck;

/***
 * This is a Marks Utility class used to Set marks,
 * Get marks , show ranks of different Exams
 * @author Batch-C
 *
 */
public class SetMpcMarks {
	// creating a list for MpC marks
	static List<MpcMarks> MPClist = new ArrayList<MpcMarks>();
	
	/**
	 * This method checks whether student attended exam or not
	 * @param examChoice
	 * @throws FileNotFoundException
	 */
	public void setMarks(int examChoice) throws FileNotFoundException {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the student id");
		long stud_id = Long.parseLong(sc.nextLine());
		if (new SetMpcMarks().checkEligible(stud_id)) {
			new SetMpcMarks().SetStudentsMarksForAllExams(stud_id, examChoice);
		} else {
			System.out.println("Student is not eligible for exam.");
		}
	}
	/**
	 * This method checks whether student eligible for exam or not
	 * @param stud_id
	 * @return true if there is a studentList in the file
	 * @throws FileNotFoundException
	 */
	public boolean checkEligible(long stud_id) throws FileNotFoundException {
		List<AcademicInfo> list = new ArrayList<AcademicInfo>();
		list = EligibleCheck.getAcademicList();
		if (list.size() == 0) {
			System.out.println("Student List is Empty");
		} else {
			Iterator<AcademicInfo> it = list.iterator();
			while (it.hasNext()) {
				AcademicInfo st1 = (AcademicInfo) it.next();
				if (stud_id == st1.getStudent_id()) {
					if (st1.getCheck().equalsIgnoreCase("yes")) {
						return true;
					}
				}
			}
		}
		return false;
	}
	/**
	 * This method set type of exam student should attend and as per 
	 * exam it set the file path to store mark sheet
	 * @param stud_id
	 * @param examChoice
	 * @throws FileNotFoundException
	 */
	public void SetStudentsMarksForAllExams(long stud_id, int examChoice) throws FileNotFoundException {
		
		String filePath = null;
		String[] studentInfo = null;
		try {
			studentInfo = getStudentNameBranch(stud_id);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		// setting text file path to store records in file
		if (examChoice == 1 && studentInfo[2].equalsIgnoreCase("mpc")) {
			filePath = "ExamTrackingDataFiles\\MPCmid1marks";
		} else if (examChoice == 1 && studentInfo[2].equalsIgnoreCase("bpc")) {
			filePath = "ExamTrackingDataFiles\\BPCmid1marks";
		} else if (examChoice == 2 && studentInfo[2].equalsIgnoreCase("mpc")) {
			filePath = "ExamTrackingDataFiles\\MPCmid2marks";
		} else if (examChoice == 2 && studentInfo[2].equalsIgnoreCase("bpc")) {
			filePath = "ExamTrackingDataFiles\\BPCmid2marks";
		} else if (examChoice == 3 && studentInfo[2].equalsIgnoreCase("mpc")) {
			filePath = "ExamTrackingDataFiles\\MPCFinalmarks";
		} else if (examChoice == 3 && studentInfo[2].equalsIgnoreCase("bpc")) {
			filePath = "ExamTrackingDataFiles\\BPCFinalmarks";
		}
		
		if(checkMarkList(stud_id,filePath)) {
			System.out.println("Student Marks Sheet Already created");
		} else {
			setMarkSheetForStudent(filePath , examChoice , stud_id);
		}
	}
	/**
	 * This after selecting exam this method generates marksheet
	 * @param filePath
	 * @param examChoice
	 * @param stud_id
	 */
	@SuppressWarnings("unchecked")
	public void setMarkSheetForStudent(String filePath,int examChoice, long stud_id) {
		@SuppressWarnings("rawtypes")
		List list = null;
		try {
			list = getAllStudentMarksList(filePath);
		} catch (FileNotFoundException e2) {
			
			e2.printStackTrace();
		}
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String[] studentInfo = null;
		try {
			studentInfo = getStudentNameBranch(stud_id);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		MpcMarks mpc = new MpcMarks();
		if (examChoice == 3) {
			mpc.setStudent_id(stud_id);
			if (studentInfo[2].equalsIgnoreCase("mpc")) {
				System.out.println("Enter Marks for Mathematics");
			} else {
				System.out.println("Enter Marks for Biology");
			}
			int math = sc.nextInt();
			while (math <= 0 || math > 75) {
					System.out.println("Enter Mark Between 0 to 75");
					math = sc.nextInt();
				}
			mpc.setMaths(math);

			System.out.println("Enter Marks for Physics");
			int physics = sc.nextInt();
			while (true) {
				if (physics <= 0 || physics > 75) {
					System.out.println("Enter Mark Between 0 to 75");
					physics = sc.nextInt();
				} else {
					break;
				}
			}
			mpc.setPhysics(physics);

			System.out.println("Enter Marks for Chemistry");
			int chemistry = sc.nextInt();
			while (true) {
				if (chemistry <= 0 || chemistry > 75) {
					System.out.println("Enter Mark Between 0 to 75");
					chemistry = sc.nextInt();
				} else {
					break;
				}
			}
			mpc.setChemistry(chemistry);

			System.out.println("Enter Marks for English");
			int english = sc.nextInt();
			while (true) {
				if (english <= 0 || english > 75) {
					System.out.println("Enter Mark Between 0 to 75");
					english = sc.nextInt();
				} else {
					break;
				}
			}
			mpc.setEnglish(english);

			System.out.println("Enter Marks for Sanskit");
			int sanskrit = sc.nextInt();
			while (true) {
				if (sanskrit <= 0 || sanskrit > 75) {
					System.out.println("Enter Mark Between 0 to 75");
					sanskrit = sc.nextInt();
				} else {
					break;
				}
			}
			mpc.setSanskrit(sanskrit);

			int total = math + chemistry + physics + english + sanskrit;
			double percentage = (total * 100 ) / 425;
			mpc.setTotalmarks(total);
			mpc.setPercentage(percentage);
			mpc.setOutOfMarks(425);
		} else {
			mpc.setStudent_id(stud_id);
			if (studentInfo[2].equalsIgnoreCase("mpc")) {
				System.out.println("Enter Marks for Mathematics");
			} else {
				System.out.println("Enter Marks for Biology");
			}
			int math = sc.nextInt();
			while (true) {
				if (math <= 0 || math > 25) {
					System.out.println("Enter Mark Between 0 to 25");
					math = sc.nextInt();
				} else {
					break;
				}
			}
			mpc.setMaths(math);

			System.out.println("Enter Marks for Physics");
			int physics = sc.nextInt();
			while (true) {
				if (physics <= 0 || physics > 25) {
					System.out.println("Enter Mark Between 0 to 25");
					physics = sc.nextInt();
				} else {
					break;
				}
			}
			mpc.setPhysics(physics);

			System.out.println("Enter Marks for Chemistry");
			int chemistry = sc.nextInt();
			while (true) {
				if (chemistry <= 0 || chemistry > 25) {
					System.out.println("Enter Mark Between 0 to 25");
					chemistry = sc.nextInt();
				} else {
					break;
				}
			}
			mpc.setChemistry(chemistry);

			System.out.println("Enter Marks for English");
			int english = sc.nextInt();
			while (true) {
				if (english <= 0 || english > 25) {
					System.out.println("Enter Mark Between 0 to 25");
					english = sc.nextInt();
				} else {
					break;
				}
			}
			mpc.setEnglish(english);

			System.out.println("Enter Marks for Sanskit");
			int sanskrit = sc.nextInt();
			while (true) {
				if (sanskrit <= 0 || sanskrit > 25) {
					System.out.println("Enter Mark Between 0 to 25");
					sanskrit = sc.nextInt();
				} else {
					break;
				}
			}
			mpc.setSanskrit(sanskrit);

			int total = (math + chemistry + physics + english + sanskrit);
			double percentage = (total * 100) / 125;
			mpc.setTotalmarks(total);
			mpc.setPercentage(percentage);
			mpc.setOutOfMarks(125);

		}
		// common values for both branches
		int markshitId = 1;
		mpc.setMarkSheet_id(markshitId);
		mpc.setFirstName(studentInfo[0]);
		mpc.setLastName(studentInfo[1]);
		mpc.setBranch(studentInfo[2]);
		System.out.println(studentInfo[2]);

		list.add(mpc);
		System.out.println(filePath);
		try {
			saveAllStudentMarksList(list, filePath);
			System.out.println("Marks saved Successfully.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * This method used to get mark sheet from file
	 * @param filePath
	 * @return
	 * @throws FileNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public static List<MpcMarks> getAllStudentMarksList(String filePath) throws FileNotFoundException {
		FileInputStream fi = null;

		List<MpcMarks> list2 = new ArrayList<MpcMarks>();

		try {
			fi = new FileInputStream(new File(filePath));
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<MpcMarks>) oi.readObject();
		} catch (Exception e) {
			System.out.println();
		}
		return list2;
	}
	/**
	 * This method save Marks Sheets into the file
	 * @param list
	 * @param filePath
	 * @throws IOException
	 */
	public static void saveAllStudentMarksList(List<MpcMarks> list, String filePath) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File(filePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
			e.printStackTrace();
		}
		obj.writeObject(list);
	}

	/**
	 * This method checks whether mark Sheet is created or not
	 * @param id
	 * @param filePath
	 * @return
	 * @throws FileNotFoundException
	 */
	public boolean checkMarkList(long id, String filePath) throws FileNotFoundException {
		List<MpcMarks> list = new ArrayList<MpcMarks>();
		list = getAllStudentMarksList(filePath);
		Iterator<MpcMarks> it = list.iterator();
		while (it.hasNext()) {
			MpcMarks marks = (MpcMarks) it.next();
			if (id == marks.getStudent_id()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * This get Gets student basic information from master file
	 * @param id 
	 * @return First name , last name , branch
	 * @throws FileNotFoundException
	 */
	public static String[] getStudentNameBranch(long id) throws FileNotFoundException {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		List<StudentDetails> list = new ArrayList();
		list = StudentDetailsImpl.getStudentList();
		String[] stdDetails = new String[3];
		if (list.size() == 0) {
			System.out.println("Student List is Empty");
		} else {
			Iterator<StudentDetails> it = list.iterator();
			while (it.hasNext()) {
				StudentDetails st1 = (StudentDetails) it.next();
				if (st1.getStudent_id() == id) {
					stdDetails[0] = st1.getFirstname();
					stdDetails[1] = st1.getLastname();
					stdDetails[2] = st1.getBranch();
				}
			}
		}
		return stdDetails;

	}
	/**
	 * This method Displays Mark sheet by taking id as parameter
	 * @param id
	 * @throws FileNotFoundException
	 */
	public void displayStudentMarksById(int choice) throws FileNotFoundException {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String filePath = null;
		String[] studentInfo = null;
		System.out.println("Enter Student Id to View Mark Sheet");
		long stud_id = sc.nextInt();
		
			if (new SetMpcMarks().checkEligible(stud_id)) {
				try {
					studentInfo = getStudentNameBranch(stud_id);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				// setting text file path to store records in file
				if (choice== 1 && studentInfo[2].equalsIgnoreCase("mpc")) {
					filePath = "ExamTrackingDataFiles\\MPCmid1marks";
				} else if (choice == 1 && studentInfo[2].equalsIgnoreCase("bpc")) {
					filePath = "ExamTrackingDataFiles\\BPCmid1marks";
				} else if (choice== 2 && studentInfo[2].equalsIgnoreCase("mpc")) {
					filePath = "ExamTrackingDataFiles\\MPCmid2marks";
				} else if (choice == 2 && studentInfo[2].equalsIgnoreCase("bpc")) {
					filePath = "ExamTrackingDataFiles\\BPCmid2marks";
				} else if (choice== 3 && studentInfo[2].equalsIgnoreCase("mpc")) {
					filePath = "ExamTrackingDataFiles\\MPCFinalmarks";
				} else if (choice == 3 && studentInfo[2].equalsIgnoreCase("bpc")) {
					filePath = "ExamTrackingDataFiles\\BPCFinalmarks";
				}

				try {
					MPClist = getAllStudentMarksList(filePath);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				if (studentInfo[2].equalsIgnoreCase("mpc")) {
					System.out.println(" Student Mark Sheet");
					System.out.println(
							"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
					System.out.printf("%15s %15s %15s %15s %15s %15s %15s %15s %15s %15s ", "Name", "Branch",
							"Mathematics", "Chemistry", "Physics", "English", "Sanskrit", "Out Of", " Total",
							"Percentage");

				} else {
					System.out.println(" Student Mark Sheet");
					System.out.println(
							"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

					System.out.printf("%15s %15s %15s %15s %15s %15s %15s %15s %15s %15s ", "Name", "Branch", "Biology",
							"Chemistry", "Physics", "English", "Sanskrit", "Out Of", " Total", "Percentage");
				}
				System.out.println();
				System.out.println(
						"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
				Iterator<MpcMarks> it = MPClist.iterator();
				while (it.hasNext()) {
					MpcMarks marks = (MpcMarks) it.next();
					if (stud_id == marks.getStudent_id()) {
						System.out.format("%15s %15s %15s %15s %15s %15s %15s %15s %15s %15s",
								marks.getFirstName() + " " + marks.getLastName(), marks.getBranch(), marks.getMaths(),
								marks.getChemistry(), marks.getPhysics(), marks.getEnglish(), marks.getSanskrit(),
								marks.getOutOfMarks(), marks.getTotalmarks(), marks.getPercentage());
						System.out.println("\n");
					}
				}
				System.out.println(
						"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			} else {
				System.out.println("Student haven't attended Exam.");
			}
	}
	@SuppressWarnings("unchecked")
	/**
	 * This method is used to display all the marks for student according to thier branch 
	 * @param examChoice is parameter to choose the type of exam
	 */
	public void displayAllMarksSheetByExam(int examChoice) {
		String filePathMpc = null;
		String filePathBpc = null;
		@SuppressWarnings("rawtypes")
		List MpcList = new ArrayList();
		@SuppressWarnings("rawtypes")
		List BpcList = new ArrayList();
		if (examChoice == 4) {
			filePathMpc = "ExamTrackingDataFiles\\MPCmid1marks";
			filePathBpc = "ExamTrackingDataFiles\\BPCmid1marks";
		} else if (examChoice == 5) {
			filePathMpc = "ExamTrackingDataFiles\\MPCmid2marks";
			filePathBpc = "ExamTrackingDataFiles\\BPCmid2marks";
		} else if (examChoice == 6) {
			filePathMpc = "ExamTrackingDataFiles\\MPCFinalmarks";
			filePathBpc = "ExamTrackingDataFiles\\BPCFinalmarks";
		} 
		try {
			MpcList = getAllStudentMarksList(filePathMpc);
			BpcList = getAllStudentMarksList(filePathBpc);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		List<MpcMarks> combineList = new ArrayList<MpcMarks>();
		combineList.addAll(MpcList);
		combineList.addAll(BpcList);
		System.out.println(combineList.size());
		System.out.println(" Student Mark Sheet");
		System.out.println(
				"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

		System.out.printf("%15s %15s %15s %15s %15s %15s %15s %15s %15s %15s ", "Name", "Branch", "Biology",
				"Chemistry", "Physics", "English", "Sanskrit", "Out Of", " Total", "Percentage\n");
		Iterator<MpcMarks> it = combineList.iterator();
		while (it.hasNext()) {
			MpcMarks marks = (MpcMarks) it.next();
				System.out.format("%15s %15s %15s %15s %15s %15s %15s %15s %15s %15s",
						marks.getFirstName() + " " + marks.getLastName(), marks.getBranch(), marks.getMaths(),
						marks.getChemistry(), marks.getPhysics(), marks.getEnglish(), marks.getSanskrit(),
						marks.getOutOfMarks(), marks.getTotalmarks(), marks.getPercentage()+"\n");
				
			} 
		System.out.println(
				"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	}
	/**
	 * This method Gives the rank of a student
	 * @param choice
	 */
	public void displayAllStudentByRank(int choice) {
		String filePath = null;
		if(choice == 1) {
			filePath =  "ExamTrackingDataFiles\\MPCFinalmarks";
		} else {
			filePath = "ExamTrackingDataFiles\\BPCFinalmarks";
		}
		 ArrayList<MpcMarks> list = new ArrayList<MpcMarks>();
		try {
			list = (ArrayList<MpcMarks>) getAllStudentMarksList(filePath);
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}
		Collections.sort(list, MpcMarks.totalMarks);
		System.out.println(
				"-------------------------------------------------------------------------------------------");
		System.out.printf("%15s %15s %15s %15s %15s " ,"Rank" ,"Student Id" , "Name" ,"Marks" ,"Percentage" );
		System.out.println();
		System.out.println(
				"-------------------------------------------------------------------------------------------");
		for(MpcMarks marks : list) {
			int rankIndex = list.indexOf(marks) + 1;
			System.out.printf("%15s %15s %15s %15s %15s ",rankIndex,marks.getStudent_id(),marks.getFirstName() +" " +marks.getLastName(),marks.getTotalmarks(),marks.getPercentage());
			System.out.println();
		}
		System.out.println(
				"-------------------------------------------------------------------------------------------");
	}
	/**
	 * This method display individual student Rank
	 * @param Student id
	 */
	public void displayStudentRankById(long stud_id) {
		String filePath = null;
		String[] studentInfo = null;
		try {
			studentInfo = getStudentNameBranch(stud_id);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		String choice = studentInfo[2];
		if(choice.equalsIgnoreCase("mpc")) {
			filePath =  "ExamTrackingDataFiles\\MPCFinalmarks";
		} else {
			filePath = "ExamTrackingDataFiles\\BPCFinalmarks";
		}
		 ArrayList<MpcMarks> list = new ArrayList<MpcMarks>();
		try {
			list = (ArrayList<MpcMarks>) getAllStudentMarksList(filePath);
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}
		Collections.sort(list, MpcMarks.totalMarks);
		System.out.println(
				"-------------------------------------------------------------------------------------------");
		System.out.printf("%15s %15s %15s %15s %15s " ,"Rank" ,"Student Id" , "Name" ,"Marks" ,"Percentage" );
		System.out.println();
		System.out.println(
				"-------------------------------------------------------------------------------------------");
		for(MpcMarks marks : list) {
			if(marks.getStudent_id() == stud_id) {
				int rankIndex = list.indexOf(marks) + 1;
				System.out.printf("%15s %15s %15s %15s %15s ",rankIndex,marks.getStudent_id(),marks.getFirstName() +" " +marks.getLastName(),marks.getTotalmarks(),marks.getPercentage());
				System.out.println();
			}
		}
		System.out.println(
				"-------------------------------------------------------------------------------------------");
	}

}
