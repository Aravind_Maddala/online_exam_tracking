package com.dao.examination;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.bean.examination.SetMarkSheet;

public class StudentMarkSheetData {

	/**
	 * This method used to get mark sheet from file
	 * @param filePath
	 * @return
	 * @throws FileNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public static List<SetMarkSheet> getAllStudentMarksList(String filePath) throws FileNotFoundException {
		FileInputStream fi = null;

		List<SetMarkSheet> list2 = new ArrayList<SetMarkSheet>();

		try {
			fi = new FileInputStream(new File(filePath));
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<SetMarkSheet>) oi.readObject();
		} catch (Exception e) {
			System.out.println();
		}
		return list2;
	}
	/**
	 * This method save Marks Sheets into the file
	 * @param list
	 * @param filePath
	 * @throws IOException
	 */
	public static void saveAllStudentMarksList(List<SetMarkSheet> list, String filePath) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File(filePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
			e.printStackTrace();
		}
		obj.writeObject(list);
	}
}
