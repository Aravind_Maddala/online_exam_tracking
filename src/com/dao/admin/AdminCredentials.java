package com.dao.admin;
import com.bean.authentication.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * This method written the list objects in the file
 * 
 * @throws IOException
 */
public class AdminCredentials {
	
	public static void saveAdminRecord(List<AdminLoginDetails> list) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File("ExamTrackingDataFiles\\AdminDetails.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
			e.printStackTrace();
		}

		obj.writeObject(list);

	}

	/**
	 * This method recently added or updated record in file
	 * 
	 * @throws FilenotFound
	 */
	@SuppressWarnings("unchecked")
	/**
	 * This method is used to read the list objects from the file
	 */
	public static List<AdminLoginDetails> getAdminList() throws FileNotFoundException {
		FileInputStream fi = new FileInputStream(new File("ExamTrackingDataFiles\\AdminDetails.txt"));

		List<AdminLoginDetails> list2 = new ArrayList<AdminLoginDetails>();
		ObjectInputStream oi = null;
		try {
			oi = new ObjectInputStream(fi);
			list2 = (List<AdminLoginDetails>) oi.readObject();
		} catch (Exception e) {
			System.out.println();
		}
		try {
			oi.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list2;
	}

}
