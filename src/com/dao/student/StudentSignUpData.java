package com.dao.student;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.bean.authentication.SetLoginDetails;

public class StudentSignUpData {
	
	/**
	 * This method fetching student login credentials from the file
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<SetLoginDetails> getLoginDetails(){
		FileInputStream fi =null; 
		
		List<SetLoginDetails> list2 = new ArrayList<SetLoginDetails>();
		
		try{
			fi=new FileInputStream(new File("ExamTrackingDataFiles\\StudentLogins"));
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<SetLoginDetails>) oi.readObject();
		}catch(Exception e){
		   System.out.println();
		}
		return list2;
	}
	/**
	 * This method to adding and updating  listobjects into the file
	 * @param list
	 * @throws IOException
	 */
	public static void saveStudentRecord(List<SetLoginDetails> list) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File("ExamTrackingDataFiles\\StudentLogins"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			 obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		obj.writeObject(list);
		
		System.out.println("Saved");
	}
}
