package com.dao.student;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.bean.student.StudentDetails;

public class StudentData {
	
	/**
	 * This method return List Object Stored in file
	 * 
	 * @throws FileNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	public static List<StudentDetails> getStudentList() throws FileNotFoundException {
		FileInputStream fi = new FileInputStream(new File("ExamTrackingDataFiles\\Std_Data.ser"));
		
		List<StudentDetails> list2 = new ArrayList<StudentDetails>();
		
		try{
			@SuppressWarnings("resource")
			ObjectInputStream oi = new ObjectInputStream(fi);
			list2 = (List<StudentDetails>) oi.readObject();
		}catch(Exception e){
		   System.out.println();
		}
		
		return list2;
	}
	
	/**
	 * This method recently added or updated record in file
	 * 
	 * @throws IOException 
	 */
	public static void saveStudentRecord(List<StudentDetails> list) throws IOException {
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(new File("ExamTrackingDataFiles\\Std_Data.ser"));
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		}
		ObjectOutputStream obj = null;
		try {
			 obj = new ObjectOutputStream(fout);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		obj.writeObject(list);
		
	}
}
