package com.view.admin;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.bean.authentication.SetLoginDetails;
import com.dao.student.StudentSignUpData;



public class DisplaySignUpStudentInfo {
	 static List<SetLoginDetails> list = new ArrayList<SetLoginDetails>();
	/**
	 * This method is used to display the studentDetails
	 *   
	 */
	public static void printLoginAllRecords() throws FileNotFoundException {
		list= StudentSignUpData.getLoginDetails();
		if(list.size() == 0) {
			System.out.println("Student List is Empty");
		} else {
			Iterator<SetLoginDetails> it = list.iterator();
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	        System.out.printf("%5s %28s %28s %28s", "Student ID", "user name","password","secuirity question");
	        System.out.println();
	        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	        while (it.hasNext()) {
	        	SetLoginDetails st1 = (SetLoginDetails)it.next();
	            System.out.format("%5s %30s %30s %30s",st1.getStud_id(),st1.getUsername(),st1.getPassword(),st1.getSecurityQues());
	            System.out.println();
	        }
	        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		}
		
	}
}
