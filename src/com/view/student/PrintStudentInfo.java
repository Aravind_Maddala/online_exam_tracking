package com.view.student;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.bean.student.StudentDetails;
import com.dao.student.StudentData;


public class PrintStudentInfo {
	static List<StudentDetails> list = new ArrayList<StudentDetails>();
	/**
	 * This method print complete Student list from file
	 * 
	 * @throws FileNotFoundException 
	 */
	public static void printStudentAllRecords() throws FileNotFoundException {
		list= StudentData.getStudentList();
		if(list.size() == 0) {
			System.out.println("Student List is Empty");
		} else {
			Iterator<StudentDetails> it = list.iterator();
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	        System.out.printf("%5s %28s %28s %28s %20s %20s %30s %15s %20s %20s", "Student ID", "First Name", "Last Name", "Email-Id", "Mobile","Parent Mobile" ,"Address" ,"Branch","DOB","DOJ");
	        System.out.println();
	        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	        while (it.hasNext()) {
	        	StudentDetails st1 = (StudentDetails)it.next();
	            System.out.format("%5s %30s %30s %30s %20s %20s %30s %15s %20s %20s",st1.getStudent_id() ,st1.getFirstname() ,st1.getLastname(), st1.getEmail_id(), st1.getMobile_no(),st1.getParent_no(),st1.getAddress(),st1.getBranch(),st1.getDate_of_birth(),st1.getDate_of_joining());
	            System.out.println();
	        }
	        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		}
		
	}
}
