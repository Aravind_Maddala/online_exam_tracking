package com.view.examination;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import com.bean.examination.SetMarkSheet;
import com.dao.examination.StudentMarkSheetData;
import com.service.examination.SetMarkSheetForAllExams;


public class DisplayMarkSheetOfAllExams {
	static List<SetMarkSheet> MPClist = new ArrayList<SetMarkSheet>();
	/**
	 * This method Displays Mark sheet by taking id as parameter
	 * @param id
	 * @throws FileNotFoundException
	 */
	public void displayStudentMarksById(int choice) throws FileNotFoundException {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		String filePath = null;
		String[] studentInfo = null;
		System.out.println("Enter Student Id to View Mark Sheet");
		long stud_id=0;
		try {
		stud_id = sc.nextInt();
		}
		catch(Exception e)
		{
			System.out.println("please enter the valid choice");
		}
			if (new SetMarkSheetForAllExams().checkEligible(stud_id)) {
				try {
					studentInfo = SetMarkSheetForAllExams.getStudentNameBranch(stud_id);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				// setting text file path to store records in file
				if (choice== 1 && studentInfo[2].equalsIgnoreCase("mpc")) {
					filePath = "ExamTrackingDataFiles\\MPCmid1marks";
				} else if (choice == 1 && studentInfo[2].equalsIgnoreCase("bpc")) {
					filePath = "ExamTrackingDataFiles\\BPCmid1marks";
				} else if (choice== 2 && studentInfo[2].equalsIgnoreCase("mpc")) {
					filePath = "ExamTrackingDataFiles\\MPCmid2marks";
				} else if (choice == 2 && studentInfo[2].equalsIgnoreCase("bpc")) {
					filePath = "ExamTrackingDataFiles\\BPCmid2marks";
				} else if (choice== 3 && studentInfo[2].equalsIgnoreCase("mpc")) {
					filePath = "ExamTrackingDataFiles\\MPCFinalmarks";
				} else if (choice == 3 && studentInfo[2].equalsIgnoreCase("bpc")) {
					filePath = "ExamTrackingDataFiles\\BPCFinalmarks";
				}

				try {
					MPClist = StudentMarkSheetData.getAllStudentMarksList(filePath);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				if (studentInfo[2].equalsIgnoreCase("mpc")) {
					System.out.println(" Student Mark Sheet");
					System.out.println(
							"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
					System.out.printf("%15s %15s %15s %15s %15s %15s %15s %15s %15s %15s ", "Name", "Branch",
							"Mathematics", "Chemistry", "Physics", "English", "Sanskrit", "Out Of", " Total",
							"Percentage");

				} else {
					System.out.println(" Student Mark Sheet");
					System.out.println(
							"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

					System.out.printf("%15s %15s %15s %15s %15s %15s %15s %15s %15s %15s ", "Name", "Branch", "Biology",
							"Chemistry", "Physics", "English", "Sanskrit", "Out Of", " Total", "Percentage");
				}
				System.out.println();
				System.out.println(
						"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
				Iterator<SetMarkSheet> it = MPClist.iterator();
				while (it.hasNext()) {
					SetMarkSheet marks = (SetMarkSheet) it.next();
					if (stud_id == marks.getStudent_id()) {
						System.out.format("%15s %15s %15s %15s %15s %15s %15s %15s %15s %15s",
								marks.getFirstName() + " " + marks.getLastName(), marks.getBranch(), marks.getMaths(),
								marks.getChemistry(), marks.getPhysics(), marks.getEnglish(), marks.getSanskrit(),
								marks.getOutOfMarks(), marks.getTotalmarks(), marks.getPercentage());
						System.out.println("\n");
					}
				}
				System.out.println(
						"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			} else {
				System.out.println("Student haven't attended Exam.");
			}
	}
	
	/**
	 * This method is used to display all the marks for student according to thier branch 
	 * @param examChoice is parameter to choose the type of exam
	 */
	public void displayAllMarksSheetByExam(int examChoice) {
		String filePathMpc = null;
		String filePathBpc = null;
		@SuppressWarnings("rawtypes")
		List MpcList = new ArrayList();
		@SuppressWarnings("rawtypes")
		List BpcList = new ArrayList();
		if (examChoice == 4) {
			filePathMpc = "ExamTrackingDataFiles\\MPCmid1marks";
			filePathBpc = "ExamTrackingDataFiles\\BPCmid1marks";
		} else if (examChoice == 5) {
			filePathMpc = "ExamTrackingDataFiles\\MPCmid2marks";
			filePathBpc = "ExamTrackingDataFiles\\BPCmid2marks";
		} else if (examChoice == 6) {
			filePathMpc = "ExamTrackingDataFiles\\MPCFinalmarks";
			filePathBpc = "ExamTrackingDataFiles\\BPCFinalmarks";
		} 
		try {
			MpcList = StudentMarkSheetData.getAllStudentMarksList(filePathMpc);
			BpcList = StudentMarkSheetData.getAllStudentMarksList(filePathBpc);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		List<SetMarkSheet> combineList = new ArrayList<SetMarkSheet>();
		combineList.addAll(MpcList);
		combineList.addAll(BpcList);
		System.out.println(combineList.size());
		System.out.println(" Student Mark Sheet");
		System.out.println(
				"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

		System.out.printf("%15s %15s %15s %15s %15s %15s %15s %15s %15s %15s ", "Name", "Branch", "Biology",
				"Chemistry", "Physics", "English", "Sanskrit", "Out Of", " Total", "Percentage\n");
		Iterator<SetMarkSheet> it = combineList.iterator();
		while (it.hasNext()) {
			SetMarkSheet marks = (SetMarkSheet) it.next();
				System.out.format("%15s %15s %15s %15s %15s %15s %15s %15s %15s %15s",
						marks.getFirstName() + " " + marks.getLastName(), marks.getBranch(), marks.getMaths(),
						marks.getChemistry(), marks.getPhysics(), marks.getEnglish(), marks.getSanskrit(),
						marks.getOutOfMarks(), marks.getTotalmarks(), marks.getPercentage()+"\n");
				
			} 
		System.out.println(
				"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
	}
	
	/**
	 * This method Gives the rank of a student
	 * @param choice
	 */
	public void displayAllStudentByRank(int choice) {
		String filePath = null;
		if(choice == 1) {
			filePath =  "ExamTrackingDataFiles\\MPCFinalmarks";
		} else {
			filePath = "ExamTrackingDataFiles\\BPCFinalmarks";
		}
		 ArrayList<SetMarkSheet> list = new ArrayList<SetMarkSheet>();
		try {
			list = (ArrayList<SetMarkSheet>) StudentMarkSheetData.getAllStudentMarksList(filePath);
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}
		Collections.sort(list, SetMarkSheet.totalMarks);
		System.out.println(
				"-------------------------------------------------------------------------------------------");
		System.out.printf("%15s %15s %15s %15s %15s " ,"Rank" ,"Student Id" , "Name" ,"Marks" ,"Percentage" );
		System.out.println();
		System.out.println(
				"-------------------------------------------------------------------------------------------");
		for(SetMarkSheet marks : list) {
			int rankIndex = list.indexOf(marks) + 1;
			System.out.printf("%15s %15s %15s %15s %15s ",rankIndex,marks.getStudent_id(),marks.getFirstName() +" " +marks.getLastName(),marks.getTotalmarks(),marks.getPercentage());
			System.out.println();
		}
		System.out.println(
				"-------------------------------------------------------------------------------------------");
	}
	/**
	 * This method display individual student Rank
	 * @param Student id
	 */
	public void displayStudentRankById(long stud_id) {
		String filePath = null;
		String[] studentInfo = null;
		try {
			studentInfo = SetMarkSheetForAllExams.getStudentNameBranch(stud_id);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		String choice = studentInfo[2];
		if(choice.equalsIgnoreCase("mpc")) {
			filePath =  "ExamTrackingDataFiles\\MPCFinalmarks";
		} else {
			filePath = "ExamTrackingDataFiles\\BPCFinalmarks";
		}
		 ArrayList<SetMarkSheet> list = new ArrayList<SetMarkSheet>();
		try {
			list = (ArrayList<SetMarkSheet>) StudentMarkSheetData.getAllStudentMarksList(filePath);
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}
		Collections.sort(list, SetMarkSheet.totalMarks);
		System.out.println(
				"-------------------------------------------------------------------------------------------");
		System.out.printf("%15s %15s %15s %15s %15s " ,"Rank" ,"Student Id" , "Name" ,"Marks" ,"Percentage" );
		System.out.println();
		System.out.println(
				"-------------------------------------------------------------------------------------------");
		for(SetMarkSheet marks : list) {
			if(marks.getStudent_id() == stud_id) {
				int rankIndex = list.indexOf(marks) + 1;
				System.out.printf("%15s %15s %15s %15s %15s ",rankIndex,marks.getStudent_id(),marks.getFirstName() +" " +marks.getLastName(),marks.getTotalmarks(),marks.getPercentage());
				System.out.println();
			}
		}
		System.out.println(
				"-------------------------------------------------------------------------------------------");
	}
}
