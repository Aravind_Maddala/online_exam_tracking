package com.view.examination;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.bean.student.AcademicInfo;
import com.dao.examination.EligibleCheckDetails;



public class PrintEligibleAllRecords{
	static List<AcademicInfo> list = new ArrayList<AcademicInfo>();
	// method to print all records in the particular student Details
	static Scanner sc=new Scanner(System.in);
	
	public static void printEligibleAllRecords() throws FileNotFoundException {
		list =EligibleCheckDetails.getAcademicList();
		if (list.size() == 0) {
			System.out.println("Student List is Empty");
		} else {
			
			System.out.println(
					"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			System.out.printf("%5s %28s %28s %25s", "Student ID", "Attendance percenatge", "fee","eligible check");
			System.out.println();
			System.out.println(
			
				"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			Iterator<AcademicInfo> it = list.iterator();
			while (it.hasNext()) {
				AcademicInfo st1 = (AcademicInfo) it.next();
				long percentage=(long) ((st1.getAttendance()*100)/150.0);
				System.out.format("%5s %30s %30s %25s ", st1.getStudent_id(), percentage, st1.getFee(),st1.getCheck());
				System.out.println();
			}
			System.out.println(
					"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		}
	}
	// method to print all student Details
		public static void printEligibleRecords() throws FileNotFoundException {
			list = EligibleCheckDetails.getAcademicList();
			System.out.println("enter the student id to view the eligibility check");
			long id = sc.nextLong();
			if (list.size() == 0) {
				System.out.println("Student List is Empty");
			} else {
				Iterator<AcademicInfo> it = list.iterator();
				while (it.hasNext()) {
					AcademicInfo st1 = (AcademicInfo) it.next();
					if (id == st1.getStudent_id()) {
						System.out.println(
								"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
						System.out.printf("%5s %28s %28s ", "Student ID", "Attendance", "fee");
						System.out.println();
						System.out.println(
								"---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
						System.out.format("%5s %30s %30s ", st1.getStudent_id(), st1.getAttendance(), st1.getFee());
					}

				}

				System.out.println(
						"\n---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			}
		}


}
