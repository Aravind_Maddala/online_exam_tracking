package com.view.examination;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.bean.examination.SetExamTimeTable;
import com.dao.examination.ExamTimeTableData;


public class DisplayExamTimeTable {
	static List<SetExamTimeTable> list = new ArrayList<SetExamTimeTable>();
	/**
	 * This method is used to Display the student timetable list
	 * @param filePath
	 * @param typeOfExam
	 * @throws FileNotFoundException
	 */
	public static void displayTimeTable(String filePath , String typeOfExam) throws FileNotFoundException {
		list = ExamTimeTableData.getExamTimeTableList(filePath);
		if(list.size() == 0) {
			System.out.println("Time Table Not Upadte");
		} else {
			
			Iterator<SetExamTimeTable> it = list.iterator();
			System.out.println("Time Table for " +typeOfExam+" Exam");
			System.out.println("---------------------------------------------------------------------------------------------------------");
	        System.out.printf("%20s %20s %20s %20s ", "Date", "Time", "Subject", "Venue");
	        System.out.println();
	        System.out.println("---------------------------------------------------------------------------------------------------------");
	        while (it.hasNext()) {
	        	SetExamTimeTable setTime = (SetExamTimeTable)it.next();
	            System.out.format("%20s %20s %20s %20s",setTime.getDate(),setTime.getTime(),setTime.getSubject(),setTime.getVenue());
	            System.out.println();
	        }
	        System.out.println("---------------------------------------------------------------------------------------------------------");
		}
	}
}
