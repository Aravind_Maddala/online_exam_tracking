package com.controller.student;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import com.controller.admin.LoginUser;
import com.controller.examination.SetMarksMenu;
import com.service.student.ResetPassword;
import com.service.student.StudentDetailsImpl;
import com.service.student.UpdateStudent;
import com.view.examination.DisplayExamTimeTable;
import com.view.examination.DisplayMarkSheetOfAllExams;
import com.view.examination.PrintEligibleAllRecords;


/**
 * 
 * @authorBATCH-'C' this class is used to login into student menu
 */
public class StudentMenu {
	/**
	 * this method is used to display the student menu
	 * 
	 * @throws IOException
	 */
	public void studentMenu() throws IOException {

		String choiceToContinue = null;
		do {
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			// Displaying Menu to student
			System.out.println("==============================");
			System.out.println("Welcome to student portal");
			System.out.println("==============================");
			System.out.println("Select operation");
			System.out.println("-------------------------------");
			System.out.println("1) Reset Password");
			System.out.println("2) View marks");
			System.out.println("3) View Rank");
			System.out.println("4) View Attendance and Fee status");
			System.out.println("5) View TimeTable");
			System.out.println("6) View and Update Personal Info");
			System.out.println("7) Go back to Main Menu");
			System.out.println("-------------------------------");
			// providing the user to enter the choice
			int choice = 0;
			try {
				choice = sc.nextInt();
			} catch (Exception e) {
				System.out.println("please enter the valid option");
				studentMenu();

			}
			switch (choice) {
			case 1: {
				// after selecting this option user can reset the paswword
				ResetPassword.checkStudentForUpdate();
				break;
			}
			case 2: {
				// after selecting this type of option user can view the marks
				SetMarksMenu.viewMarksMenu();
				break;
			}
			case 3: {

				// showing individual Rank to the student
				System.out.println("Enter the student id:");
				long std_id = sc.nextLong();
				new DisplayMarkSheetOfAllExams().displayStudentRankById(std_id);
				break;
			}
			case 4: {
				// after selecting this type of option student can view the attendance and fee
				PrintEligibleAllRecords.printEligibleRecords();
				break;

			}
			case 5: {
				// after selecting this type of option user can view the timetable
				try {
					DisplayExamTimeTable.displayTimeTable("ExamTrackingDataFiles\\MPCmid1timetable", "First Term");
					DisplayExamTimeTable.displayTimeTable("ExamTrackingDataFiles\\MPCmid2timetable", "Second Term");
					DisplayExamTimeTable.displayTimeTable("ExamTrackingDataFiles\\MPCFinaltimetable", "Final");

				} catch (FileNotFoundException e) {

					e.printStackTrace();
				}
				break;
			}
			case 6: {
				// after selecting this type of option user can update the personal info
				System.out.println("select operation");
				System.out.println("-------------------------------");
				System.out.println("1) View details");
				System.out.println("2) Update details");
				int choice1 = 0;
				try {
					choice1 = sc.nextInt();
					while (choice1 <= 0 || choice1 > 2) {
						System.out.println("please select valid option");
						choice1 = sc.nextInt();
					}
					if (choice1 == 1) {
						PrintEligibleAllRecords.printEligibleRecords();
					}
					if (choice1 == 2) {
						UpdateStudent.checkStudentForUpdate();
					}
				} catch (Exception e) {
					System.err.println("please enter integers only");
				}
				break;
			}

			case 7: {
				// after selecting this option user can go back to main menu
				new LoginUser().loginUser();

			}
			}
			// here can choose option to continue
			System.out.println("Do You want to continue(yes/no)");
			choiceToContinue = sc.next();
		} while (choiceToContinue.equalsIgnoreCase("yes"));
	}
}
