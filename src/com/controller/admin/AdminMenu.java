package com.controller.admin;

import java.io.IOException;
import java.util.Scanner;

import com.controller.examination.MasterExamMenu;
import com.service.student.SignUpStudent;
import com.service.student.StudentDetailsImpl;
import com.service.student.UpdateStudent;
import com.view.admin.DisplaySignUpStudentInfo;
import com.view.student.PrintStudentInfo;


/**
 * 
 * @author BATCH-'C'
 *
 * This class Displays Admin Functionality
 */
public class AdminMenu {
	/**
	 * This method Displays All Functions which is perform by Admin
	 * 
	 * @throws IOException
	 */
	public void adminFunctions() throws IOException {

		String choiceToContinue = null;
		do {
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in); 
			// Displaying Menu to Admin
			System.out.println("==============================");
			System.out.println("Welcome to Admin portal");
			System.out.println("==============================");
			System.out.println("select operation");
			System.out.println("-------------------------------");
			System.out.println("1) Enroll New Student");
			System.out.println("2) Update Student");
			System.out.println("3) Examination Portal");
			System.out.println("4) View All Students");
			System.out.println("5) Enroll signup for student");
			System.out.println("6) view signup details of student");
			System.out.println("7) Go back to main menu");
			System.out.println("-------------------------------");
			int choice=0;
			try {
			choice= sc.nextInt();
			while(choice<=0||choice>7) {
				System.err.println("please select valid option");
				choice= sc.nextInt();
			}
			}
			catch(Exception e) {
				System.err.println("please enter integers only");
				adminFunctions();
			}
			switch (choice) {
			case 1: {
				// creating student class object and calling method which
				// add new student in file
				
				StudentDetailsImpl enollNewStudent = new StudentDetailsImpl();
				try {
					enollNewStudent.enRollNewStudent();
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
			case 2: {
				boolean isUpdate = false;
				try {
					// this method return true if student present infile
					// if true then we are updating student information
					isUpdate = UpdateStudent.checkStudentForUpdate();
					if (isUpdate) {
						System.out.println("Student Updated Successfully.");
					} else {
						System.out.println("Student Record not Available.");
					}
				} catch (IOException e) {
					// Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
			case 3: {
				// after selection type of exam this method set exam time table
				MasterExamMenu.getMasterMenuForExam();
				break;
			}
			case 4: {
				// this method prints all student present in a list
				PrintStudentInfo.printStudentAllRecords();
				break;
			}
			case 5: {
				// if student is new to portal and if student is present in a list
				// then this method provides facility to sign up
				SignUpStudent signup = new SignUpStudent();
				signup.signupNewStudent();
				break;
			}
			case 6:{
				// after selecting this type to print all the records of the student 
				DisplaySignUpStudentInfo.printLoginAllRecords(); 
				break;

			}
			case 7:{
				// after selecting this type you can goback to login page
				new LoginUser().loginUser();
			
			}
			}
			// here can choose option to continue
			System.out.println("Do You want to continue(yes/no)");
			choiceToContinue = sc.next();
		} while (choiceToContinue.equalsIgnoreCase("yes"));

	}
}
