package com.controller.examination;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.bean.student.AcademicInfo;
import com.dao.examination.EligibleCheckDetails;
import com.dao.student.CheckIdInRegistartion;
import com.service.examination.EligibleCheck;



public class EligibleCheckLogic {
	// intiliazing scanner
	static Scanner sc = new Scanner(System.in);
	// creating a object for academic details of student
	AcademicInfo academicInfo = new AcademicInfo();
	// creating a list for Academicinfo
	static List<AcademicInfo> list = new ArrayList<AcademicInfo>();
	String check=null;

	/**
	 *  This method to view the eligible check for student
	 * @throws IOException
	 */
	public void setEligible() throws IOException {
		Scanner sc = new Scanner(System.in);
		list =EligibleCheckDetails.getAcademicList();
		System.out.println("enter the student id to set the eligibility criteria");
		long stud_id = Long.parseLong(sc.nextLine());
		boolean checkid = CheckIdInRegistartion.CheckId(stud_id);
		boolean checkid11=true;
		if (checkid) {
			Iterator<AcademicInfo> it = list.iterator();
		    while (it.hasNext()) {
			AcademicInfo st1 = (AcademicInfo) it.next();
			if(stud_id== st1.getStudent_id()) {
				checkid11=false;
				}
			}
			// to set the eligiblity check for the student 
			if(checkid11) {
			System.out.println("set the attendance for " + stud_id + "\n");
			System.out.println("enter the number of days present by student id");
			int attendance = Integer.parseInt(sc.nextLine());
			academicInfo.setAttendance(attendance);
			boolean setattendance = EligibleCheck.setAttendance(attendance);
			System.out.println("set the fee status for " + stud_id + "\n");
			String fee = sc.nextLine();
			academicInfo.setFee(fee);
			boolean setFee = EligibleCheck.setFee(fee);
			academicInfo.setStudent_id(stud_id);
			boolean check1=EligibleCheck.check(setattendance, setFee);
			if(check1) {
				check="yes";
			}
			else {
				check="no";
			}
			academicInfo.setCheck(check);
			list.add(academicInfo);
			EligibleCheckDetails.setAcademicRecord(list);
			
			if (setattendance && setFee) {
				System.out.println("student is eligible for exam");
				list = EligibleCheckDetails.getAcademicList();
			} else if (setattendance || !(setFee)) {
				System.out.println("student is not eligible for exam");

				list =EligibleCheckDetails.getAcademicList();

				System.out.println("fee is pending");

				System.out.println("oops!!!Better luck next time");

			} else if (!(setattendance) || (setFee)) {

				System.out.println("student is not eligible for exam");

				list = EligibleCheckDetails.getAcademicList();

				System.out.println("attendance is less than 50 your are not eligible for examination");

			}	

		}
			else {
				System.out.println("student status is already set");
			}
		}

		else {
			System.out.println("Id not found,please enter the correct student  id for signup");
			setEligible();
		}
	}


}
