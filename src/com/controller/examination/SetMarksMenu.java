package com.controller.examination;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import com.service.examination.SetMarkSheetForAllExams;
import com.view.examination.DisplayMarkSheetOfAllExams;

/**
 * 
 * @author BATCH-'C' 
 * This class is used to set marks menu for admin
 *
 */
public class SetMarksMenu {

	/**
	 * the method is to used to display the time table for students
	 * 
	 * @param branchChoice parameter to choose the branch
	 * @throws FileNotFoundException
	 */
	public static void SetMarksSubMenu() throws FileNotFoundException {
		String choiceToContinue = null;
		do {
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			// Displaying Menu to Admin
			System.out.println("==============================================");
			System.out.println("\nWelcome to Examination portal");
			System.out.println("==============================================");
			System.out.println("Select the Exam Type");
			System.out.println("----------------------------------------------");
			System.out.println("1) Set Marks for First Mid Term Exam");
			System.out.println("2) Set Marks for Second Mid Term Exam");
			System.out.println("3) Set Marks for Final Exam");
			System.out.println("4) View Student Marks");
			System.out.println("5) Go Back");
			System.out.println("----------------------------------------------");
			int choice = 0;
			try {
				choice = sc.nextInt();
			} catch (Exception e) {
				System.out.println("please enter the valid option");
				SetMarksSubMenu();

			}
			SetMarkSheetForAllExams setMark = new SetMarkSheetForAllExams();
			switch (choice) {
			case 1: {
				try {
					setMark.setMarks(choice);
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
			case 2: {
				try {
					setMark.setMarks(choice);
				} catch (IOException e) {
					e.printStackTrace();
				}

				break;
			}
			case 3: {
				try {
					setMark.setMarks(choice);
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
			case 4: {
				viewMarksMenu();
				break;
			}
			case 5: {
				MasterExamMenu.getMasterMenuForExam();
				break;
			}
			}
			// here can choose option to continue
			System.out.println("Do You want to continue(yes/no)");
			choiceToContinue = sc.next();
		} while (choiceToContinue.equalsIgnoreCase("yes"));
	}

	public static void viewMarksMenu() {
		String choiceToContinue = null;
		do {
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			// Displaying Menu to Admin
			System.out.println("==============================================");
			System.out.println("\nWelcome to Examination portal");
			System.out.println("==============================================");
			System.out.println("Select the Exam Type to view Marks");
			System.out.println("----------------------------------------------");
			System.out.println("1) View Marks for First Mid Term Exam By Id");
			System.out.println("2) View Marks for Second Mid Term Exam By Id");
			System.out.println("3) View Marks for Final Exam By Id");
			System.out.println("          --------------------           ");
			System.out.println("4) View All Marks for First Mid Term Exam");
			System.out.println("5) View All Marks for Second Mid Term Exam");
			System.out.println("6) View All Marks for Final Exam");
			System.out.println("7) View Rank of the Student");
			System.out.println("8) Go Back");
			System.out.println("----------------------------------------------");
			int choice = 0;
			try {
				choice = sc.nextInt();
			} catch (Exception e) {
				System.out.println("please enter the valid choice");
				viewMarksMenu();
			}

			DisplayMarkSheetOfAllExams viewMark = new DisplayMarkSheetOfAllExams();
			switch (choice) {
			case 1: {
				try {
					viewMark.displayStudentMarksById(choice);
				} catch (FileNotFoundException e) {

					e.printStackTrace();
				}
				break;
			}
			case 2: {
				try {
					viewMark.displayStudentMarksById(choice);
				} catch (FileNotFoundException e) {

					e.printStackTrace();
				}

				break;
			}
			case 3: {
				try {
					viewMark.displayStudentMarksById(choice);
				} catch (FileNotFoundException e) {

					e.printStackTrace();
				}
				break;
			}
			case 4: {
				viewMark.displayAllMarksSheetByExam(choice);
				break;
			}
			case 5: {
				viewMark.displayAllMarksSheetByExam(choice);
				break;
			}
			case 6: {
				viewMark.displayAllMarksSheetByExam(choice);
				break;
			}
			case 7: {
				new SetMarksMenu().displayRankMenu();
				break;
			}
			case 8: {
				try {
					SetMarksSubMenu();
				} catch (FileNotFoundException e) {

					e.printStackTrace();
				}
				break;
			}
			}
			// here can choose option to continue
			System.out.println("Do You want to continue(yes/no)");
			choiceToContinue = sc.next();
		} while (choiceToContinue.equalsIgnoreCase("yes"));
	}

	public void displayRankMenu() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		// Displaying Menu to Admin
		System.out.println("==============================================");
		System.out.println("\nWelcome to Examination portal");
		System.out.println("==============================================");
		System.out.println("Select the Branch to view Rank of the student");
		System.out.println("----------------------------------------------");
		System.out.println("1) MPC");
		System.out.println("2) BPC");
		System.out.println("2) Go Back");
		int choice = 0;
		try {
			choice = sc.nextInt();
		} catch (Exception e) {
			System.out.println("please enter the valid option");
			displayRankMenu();
		}
		DisplayMarkSheetOfAllExams viewMark = new DisplayMarkSheetOfAllExams();
		switch (choice) {
		case 1: {
			viewMark.displayAllStudentByRank(choice);
			break;
		}
		case 2: {
			viewMark.displayAllStudentByRank(choice);
			break;
		}
		case 3: {
			viewMarksMenu();
			break;
		}
		}
	}
}
