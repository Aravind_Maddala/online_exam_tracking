package com.bean.examination;

import java.io.Serializable;
import java.util.Comparator;

public class SetMarkSheet implements Serializable{

	/**
	 * BATCH-'C'
	 * This class is used to declare the variables foe setmarks of the student
	 */
	private static final long serialVersionUID = 1L;
	
	private long student_id;
	private int maths;
	private int physics;
	private int chemistry;
	private int english;
	private int sanskrit;
	private long markSheet_id;
	private int totalmarks;
	private int outOfMarks;
	private double percentage;
	private String firstName;
	private String lastName;
	private String branch;
	private String name;
/**
 * 
 * @return student_id
 */
	public long getStudent_id() {
		return student_id;
	}

	public void setStudent_id(long student_id) {
		this.student_id = student_id;
	}
     /**
      * 
      * @return maths
      */
	public int getMaths() {
		return maths;
	}

	public void setMaths(int maths) {
		this.maths = maths;
	}
    /**
     * 
     * @return physics
     */
	public int getPhysics() {
		return physics;
	}

	public void setPhysics(int physics) {
		this.physics = physics;
	}
    /**
     * 
     * @return chemistry
     */
	public int getChemistry() {
		return chemistry;
	}

	public void setChemistry(int chemistry) {
		this.chemistry = chemistry;
	}
    /**
     * 
     * @return english
     */
	public long getEnglish() {
		return english;
	}

	public void setEnglish(int english) {
		this.english = english;
	}
    /**
     * 
     * @return sanskrit
     */
	public int getSanskrit() {
		return sanskrit;
	}

	public void setSanskrit(int sanskrit) {
		this.sanskrit = sanskrit;
	}
     /**
      * 
      * @return marksheet_id
      */
	public long getMarkSheet_id() {
		return markSheet_id;
	}

	public void setMarkSheet_id(long markSheet_id) {
		this.markSheet_id = markSheet_id;
	}
    /**
     * 
     * @return totalmarks
     */
	public int getTotalmarks() {
		return totalmarks;
	}

	public void setTotalmarks(int totalmarks) {
		this.totalmarks = totalmarks;
	}
    /**
     * 
     * @return percentage
     */
	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
    /**
     * 
     * @return  out ofMarks
     */
	public int getOutOfMarks() {
		return outOfMarks;
	}

	public void setOutOfMarks(int outOfMarks) {
		this.outOfMarks = outOfMarks;
	}
     /**
      * 
      * @return name
      */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    /**
     * 
     * @return firstname
     */
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
    /**
     * 
     * @return lastname
     */
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
    /**
     * 
     * @return branch
     */
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	/***
	 * This static class ordered student in descending order 
	 */
	public static Comparator<SetMarkSheet> totalMarks = new Comparator<SetMarkSheet>() {

		public int compare(SetMarkSheet s1, SetMarkSheet s2) {

		   int marks1 = s1.getTotalmarks();
		   int marks2 = s2.getTotalmarks();

		   /*For ascending order*/
		   return marks2 - marks1;
	   }};

}