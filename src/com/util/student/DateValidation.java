package com.util.student;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class DateValidation {
public String isThisDateValid(int choice){
	String date_to_validate1=null;
	@SuppressWarnings("resource")
	Scanner sc = new Scanner(System.in);
	 try { 
		 String dateToValidate=sc.next();
	    Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(dateToValidate);  
	    Date date=new Date();
	    if(choice==1) {
	    if(date.after(date1)) {
	    	return dateToValidate ;
	    }
	    else {
			System.out.println("please enter date before todays date");
	    	isThisDateValid(choice);
	    }
	    }
	    if(choice==2) {
	    	if(date.before(date1)) {
	    	return dateToValidate;
	    }
	    else {
	    	System.out.println("please enter date after todays date");
	    	isThisDateValid(choice);
	    }
	    }
         }
        catch(Exception e){
	          System.out.println("please enter correct valid date format");
	          date_to_validate1= isThisDateValid(choice);
         }
	return date_to_validate1;
}
}