package com.util.student;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.bean.student.StudentDetails;
import com.dao.student.StudentData;


public class StudentUtility {
	 static List<StudentDetails> list = new ArrayList<StudentDetails>();
	/**
	 * This method gives Auto Generated Student id to insert record
	 * 
	 * @throws FileNotFoundException 
	 */
	public static long getNextStudentId() {
		long std_id = 101;
		try {
			list = StudentData.getStudentList();
			if(list.size() == 0) {
				std_id = 101;
			} else {
				StudentDetails student = new StudentDetails();
				student = list.get(list.size() - 1);
				std_id = (student.getStudent_id() + 1);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return std_id;   
	}
}
