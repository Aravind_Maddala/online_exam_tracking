package com.util.student;
import java.util.regex.*;
public class MobileEmailValidation {
	/**
	 * This method checks whether Given mobile number is valid or not
	 */
	public boolean isValidMobileNumber(String s) { 
        Pattern p = Pattern.compile("(0/91)?[7-9][0-9]{9}"); 
        Matcher m = p.matcher(s); 
        return (m.find() && m.group().equals(s)); 
    }
	
	/**
	 * This method checks whether Given Email Id is valid or not
	 */
	public boolean isValidEmailId(String email) { 
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" +  "A-Z]{2,7}$";                     
        Pattern pat = Pattern.compile(emailRegex); 
        if (email == null) 
            return false; 
        return pat.matcher(email).matches(); 
    } 
	
	  
	  /**
	   * Validate password with regular expression
	   * @param password password for validation
	   * @return true valid password, false invalid password
	   */
	  public boolean validatePassword(String password){
		  final String PASSWORD_PATTERN = 
		            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
		  Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
		  Matcher matcher = pattern.matcher(password);
		  return matcher.matches();
	    	    
	  }
	
}
