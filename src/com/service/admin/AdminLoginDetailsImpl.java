package com.service.admin;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.bean.authentication.*;
import com.controller.admin.AdminMenu;
import com.dao.admin.AdminCredentials;

public class AdminLoginDetailsImpl {
	// list to set and get the admin login details from file
	static List<AdminLoginDetails> list = new ArrayList<AdminLoginDetails>();
	static int count = 0;

	/**
	 ** This method is for setting hard-coded values for admin
	 * @throws IOException
	 */
	public void setAdminDetails() throws IOException {
	
        
		AdminLoginDetails adminLogin = new AdminLoginDetails();
		adminLogin.setUserName("varun");
		adminLogin.setPassword("varun123");
		adminLogin.setSecurityQues("black");
		list.add(adminLogin);

		AdminLoginDetails adminLogin1 = new AdminLoginDetails();
		adminLogin1.setUserName("nilesh");
		adminLogin1.setPassword("nilesh123");
		adminLogin1.setSecurityQues("maroon");
		list.add(adminLogin1);

		AdminLoginDetails adminLogin2 = new AdminLoginDetails();
		adminLogin2.setUserName("megha");
		adminLogin2.setPassword("megha123");
		adminLogin2.setSecurityQues("blue");
		list.add(adminLogin2);

		AdminLoginDetails adminLogin3 = new AdminLoginDetails();
		adminLogin3.setUserName("aravind");
		adminLogin3.setPassword("aravind123");
		adminLogin3.setSecurityQues("white");
		list.add(adminLogin3);

		AdminLoginDetails adminLogin4 = new AdminLoginDetails();
		adminLogin4.setUserName("vineesha");
		adminLogin4.setPassword("vineesha123");
		adminLogin4.setSecurityQues("green");
		list.add(adminLogin4);

		AdminCredentials.saveAdminRecord(list);
		// getting list from file
		list = AdminCredentials.getAdminList();
		Scanner sc=new Scanner(System.in);
		// Validation checking
		System.out.println("enter the user name");
		String userName = sc.nextLine();
		System.out.println("enter the password");
		String password = sc.nextLine();
	
		Iterator<AdminLoginDetails> itr = list.iterator();
		
		while (itr.hasNext()) {
			AdminLoginDetails admin1 = (AdminLoginDetails) itr.next();
			if (userName.equalsIgnoreCase(admin1.getUserName()) && password.equalsIgnoreCase(admin1.getPassword())) {
				System.out.println("Hi "+userName);
				//Creating Admin object for performing admin functions
				AdminMenu menu = new AdminMenu();
				menu.adminFunctions();
				System.exit(0);
			} else {
				count++;
			}
		}
		// Object creation, if user not entered correct credentials
				AdminLoginDetails admin2 = new AdminLoginDetails();

				if (!(userName.equalsIgnoreCase(admin2.getUserName())) || !(password.equalsIgnoreCase(admin2.getPassword())))
					if (count <= 6) {
						for (int i = 0; i < 3; i++) {
							System.out.println("please enter the correct details");
							setAdminDetails();
						}

						Iterator<AdminLoginDetails> itr2 = list.iterator();
						
						while (itr2.hasNext()) {
							AdminLoginDetails admin3 = (AdminLoginDetails) itr2.next();
							
							for (int j = 0; j < 3; j++) {
								System.out.println("please enter the correct user name to get the credentials");
								userName = sc.nextLine();
								
								System.out.println("please answer the security question");
								System.out.println("please enter your favorite color");
								
								String securityQuestion = sc.nextLine();
								
								if (userName.equalsIgnoreCase(admin3.getUserName())
										&& securityQuestion.equalsIgnoreCase(admin3.getSecurityQues())) {
									System.out.println("Your username is : " + admin3.getUserName());
									System.out.println("Your password is : " + admin3.getPassword());
									setAdminDetails();
								}
							}
							
							if (!(userName.equalsIgnoreCase(admin2.getUserName()))
									|| !(password.equalsIgnoreCase(admin2.getPassword()))) {
								System.out.println("Too many attempts!!! please try again after sometime");
								System.exit(0);
							}
						}
					}
//				sc.close();
			}

	}


