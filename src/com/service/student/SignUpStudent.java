package com.service.student;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.bean.authentication.SetLoginDetails;
import com.dao.student.CheckIdInLogins;
import com.dao.student.CheckIdInRegistartion;
import com.dao.student.StudentSignUpData;
import com.util.student.MobileEmailValidation;


public class SignUpStudent {

	 static List<SetLoginDetails> list = new ArrayList<SetLoginDetails>();
	 /**
	 * @throws IOException 
	 *  This method used to create student login credentials
	 */
	public void signupNewStudent() throws IOException {
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		//getting list from file
		 list = StudentSignUpData.getLoginDetails();
		 
		
		//getting id for setting password to student
		System.out.println("enter the student id to set the password");
		long stud_id=sc.nextLong();
		boolean checkid1=CheckIdInLogins.CheckId1(stud_id);
		boolean checkid=CheckIdInRegistartion.CheckId(stud_id);
		if(list.size()>=0){
			if(checkid1)
			{
			System.out.println("student details already entered");	
		    }
			else {
				//checking whether student present or not
				if(checkid) {	
				SetLoginDetails student = new SetLoginDetails();
				
				//if student present , then setting new credentials to student
				System.out.println("Enter the username");
				String userName = sc.next();
				student.setUsername(userName);
				// validating the password for student
				MobileEmailValidation validate = new MobileEmailValidation();
				System.out.println("Enter the password");
				String password = sc.next();
				boolean flag = true;
				do {
					if(validate.validatePassword(password)) {
						student.setPassword(password);
						flag = false;
						break;
					} else {
						System.out.println("Password must contains following Characters");
						System.out.println("One lowercase , one uppercase , one special symbol , one number and length between 6 to 20");
						password = sc.next();
					}
				}while(flag);
				student.setPassword(password);

				String securityQues = "***";
				student.setSecurityQues(securityQues);
				student.setStud_id(stud_id);
				list.add(student);
				StudentSignUpData.saveStudentRecord(list);
				}
				else {
					System.out.println("Id not found,please enter the correct student  id for signup");
					signupNewStudent();
				}    
			
			}
		}
		else {
			System.out.println("list is empty");
		}
		
	}


}
	
