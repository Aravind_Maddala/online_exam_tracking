package com.service.student;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.*;
import com.bean.authentication.*;
import com.controller.student.StudentMenu;
import com.dao.student.StudentSignUpData;
public class StudentLogins {
	static List<SetLoginDetails> list = new ArrayList<SetLoginDetails>();
	static int count = 0;

	 @SuppressWarnings("unused")
	 /**
	  * This  method to check the student credentials whether it is correct or not 
	  * @throws IOException
	  */
	public void studentLoginDetails() throws IOException {
		Scanner sc = new Scanner(System.in);
		SignUpStudent signupStudents = new SignUpStudent();
		list = StudentSignUpData.getLoginDetails();
		
		// Validation checking
		System.out.println("enter the user name");
		String userName = sc.nextLine();
		System.out.println("enter the password");
		String password = sc.nextLine();
		Iterator<SetLoginDetails> itr = list.iterator();

		// checking the credentials
		while (itr.hasNext()) {
			SetLoginDetails admin1 = (SetLoginDetails) itr.next();
			if (userName.equalsIgnoreCase(admin1.getUsername()) && password.equalsIgnoreCase(admin1.getPassword())) {
				System.out.println("success");
				StudentMenu menu = new StudentMenu();
				menu.studentMenu();
				System.exit(0);
			} else {
				count++;
			}
		}

		// Object creation, if user not entered correct credentials
		SetLoginDetails admin2 = new SetLoginDetails();

		if (!(userName.equalsIgnoreCase(admin2.getUsername())) || !(password.equalsIgnoreCase(admin2.getPassword())))
			if (count <= 100) {
				for (int i = 0; i < 3; i++) {
					System.out.println("please enter the correct details");
					studentLoginDetails();
					break;
				}
                Iterator<com.bean.authentication.SetLoginDetails> itr2 = list.iterator();
				while (itr2.hasNext()) {
					SetLoginDetails admin3 = (SetLoginDetails) itr2.next();
					for (int j = 0; j < 3; j++) {
						System.out.println("please enter the correct user name to get the credentials");
						userName = sc.nextLine();
						System.out.println("please answer the security question");
						System.out.println("please enter your favorite color");
						String securityQuestion = sc.nextLine();
						if (userName.equalsIgnoreCase(admin3.getUsername())
								&& securityQuestion.equalsIgnoreCase(admin3.getSecurityQues())) {
							System.out.println("Your username is : " + admin3.getUsername());
							System.out.println("Your password is : " + admin3.getPassword());
							studentLoginDetails();
							break;
						}
					}
					if (!(userName.equalsIgnoreCase(admin2.getUsername()))
							|| !(password.equalsIgnoreCase(admin2.getPassword()))) {
						System.out.println("Too many attempts!!! please try again after sometime");
						System.exit(0);
					}
				}
			}
		sc.close();
	}
}
