package com.service.student;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import com.bean.authentication.SetLoginDetails;
import com.dao.student.StudentSignUpData;
import com.util.student.MobileEmailValidation;
public class ResetPassword {
	 static  long id=0;
		// creating the list for the SetloginDetails
		static List<SetLoginDetails> list = new ArrayList<SetLoginDetails>();
	   /**
	    * This methnod is used to check the whether trhe student id is correct or not
	    * @throws IOException
	    */
      public static void checkStudentForUpdate() throws IOException {
		Scanner sc = new Scanner(System.in);
		SetLoginDetails st1=null;
		try {
			list = StudentSignUpData.getLoginDetails();
			Iterator<SetLoginDetails> it = list.iterator();
			System.out.println("Enter Student id to update");
		     id = Long.parseLong(sc.nextLine());
	        while (it.hasNext()) {
	        	st1 = (SetLoginDetails)it.next();
	        	if(st1.getStud_id() == id) {
	        		resetPassword(st1 , list, sc);	
	        		break;
	        	}
	        }
	    	if(!(st1.getStud_id() == id)) {
	    		System.out.println("please enter correct id");
	    		checkStudentForUpdate();
	    	}
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
	}
	/**
	 * This method is reset the password when user forgets the passsword
	 * @param student
	 * @param list
	 * @param sc
	 * @throws IOException
	 */
			public static  void resetPassword(SetLoginDetails student , List<SetLoginDetails> list, Scanner sc) throws IOException {
				System.out.println("reset the password");
		        
			System.out.println("please enter the previous user name");
			String username=sc.nextLine();
			System.out.println("please enter the previous password");
			String password=sc.nextLine();
			
			Iterator<SetLoginDetails> itr=list.iterator();
			while(itr.hasNext()) {
				SetLoginDetails std=(SetLoginDetails)itr.next();
				     if(std.getStud_id() == id) {
					if(username.equalsIgnoreCase(std.getUsername())&&password.equalsIgnoreCase(std.getPassword())){
						System.out.println("please enter the new user name");
						String username1=sc.nextLine();
						student.setUsername(username1);
						System.out.println("please enter the new password");
						// validating the password for student
						MobileEmailValidation validate = new MobileEmailValidation();
						password = sc.nextLine();
						student.setPassword(password);
						boolean flag = true;
						do {
							if(validate.validatePassword(password)) {
								student.setPassword(password);
								flag = false;
								break;
							} else {
								System.out.println("Password must contains following Characters");
								System.out.println("One lowercase , one uppercase , one special symbol , one number and length between 6 to 20");
								password = sc.next();
							}
						}while(flag);
						student.setPassword(password);
					
						System.out.println("please answer the security question");
						System.out.println("please enter your favorite color");
						String security=sc.nextLine();
						student.setSecurityQues(security);
					
						try {
							StudentSignUpData.saveStudentRecord(list);
						} catch (IOException e) {
							
							e.printStackTrace();
						}
						
						list = StudentSignUpData.getLoginDetails();
						System.out.println(student.getStud_id() + " password reset Successfully.");
						
					}
				 }
			}		
		}
}
