package com.service.student;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bean.student.StudentDetails;
import com.dao.student.StudentData;
import com.util.student.DateValidation;
import com.util.student.MobileEmailValidation;
import com.util.student.StudentUtility;

/**
 * 
 * @author Batch-C
 *
 */

public class StudentDetailsImpl {

	static List<StudentDetails> list = new ArrayList<StudentDetails>();
	 
	 /**
	 * This method Used to Add new Student Record
	 *
	 * @throws IOException 
	 */
	public void enRollNewStudent() throws IOException {
		
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		list = StudentData.getStudentList();
		
		StudentDetails student = new StudentDetails();
		MobileEmailValidation validate = new MobileEmailValidation();
		
		long student_id = StudentUtility.getNextStudentId();
		student.setStudent_id(student_id);
		
		System.out.println("Enter the First Name");
		String firstName = sc.next();
		student.setFirstname(firstName);
		
		System.out.println("Enter the Last Name");
		String lastName = sc.next();
		student.setLastname(lastName);
		
		System.out.println("Enter the emailid");
		String email_id = sc.next();
		boolean flag = true;
		do {
			if(validate.isValidEmailId(email_id)) {
				student.setEmail_id(email_id);
				flag = false;
				break;
			} else {
				System.out.println("Please Enter valid Email Address");
				email_id = sc.next();
			}
		}while(flag);
		
		
		System.out.println("Enter the Mobile Number");
		String mobile_no= sc.next();
		boolean flag1 = true;
		do {
			if(validate.isValidMobileNumber(mobile_no)) {
				student.setMobile_no(mobile_no);
				flag1 = false;
				break;
			} else {
				System.out.println("Please Enter Correct Mobile Number");
				mobile_no= sc.next();
			}
		}while(flag1);
		
		
		System.out.println("Enter the parent no");
		String parent_no= sc.next();
		boolean flag2 = true;
		do {
			if(validate.isValidMobileNumber(parent_no)) {
				student.setParent_no(parent_no);
				flag2 = false;
				break;
			} else {
				System.out.println("Please Enter Correct Mobile Number");
				parent_no= sc.next();
			}
		}while(flag2);
		
		
		System.out.println("Enter the address");
		String address= sc.next();
		student.setAddress(address);
		
		System.out.println("Enter the branch(mpc/bpc)");
		String branch= sc.next();
		while(!(branch.equalsIgnoreCase("mpc"))&&!(branch.equalsIgnoreCase("bpc")))
		{
			System.out.println("please enter valid branch(MPC/BPC)");
			 branch= sc.next();
		}
		student.setBranch(branch);
	
		
		System.out.println("Enter the date of birth (DD/MM/YYYY)");
       DateValidation datevalid=new DateValidation();
       int choice=1;
       String date_of_birth=datevalid.isThisDateValid(choice);
       student.setDate_of_birth(date_of_birth);
		System.out.println("Enter the date of joining");
		String date_of_joining=datevalid.isThisDateValid(choice);
		student.setDate_of_joining(date_of_joining);
		
		System.out.println("New Student Added Successfully.");
		
		list.add(student);
		StudentData.saveStudentRecord(list);
	
	}
}
