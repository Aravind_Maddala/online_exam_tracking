package com.service.examination;

import java.util.Scanner;

public class EligibleCheck {
   static Scanner sc=new Scanner(System.in);
	/**
	 *  This method for checking attendance for eligibility of exam or not
	 * @param attendance
	 * @return
	 */
	public static boolean setAttendance(long attendance) {

		while (attendance < 0 || attendance > 150) {
			System.out.println("please enter the valid number of days present by student");
			attendance = Integer.parseInt(sc.nextLine());
		}
		double percentage = (attendance * 100.0) / 150;
		System.out.println("attendance:" + percentage);
		if (percentage > 50) {

			return true;
		}

		return false;

	}
    /**
     *  This method 
     * @param fee
     * @return
     */
	public static boolean setFee(String fee) { // method to check fee status of a student eligiblity of exam or no
		if (fee.equalsIgnoreCase("paid")) {
			return true;
		}
		return false;
	}
	public static boolean  check(boolean setattendance,boolean setFee) {
		if (setattendance && setFee) {
			return true;
		}
		return false;
	}

}
