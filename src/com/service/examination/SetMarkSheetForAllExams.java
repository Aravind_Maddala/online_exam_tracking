package com.service.examination;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import com.bean.examination.SetMarkSheet;
import com.bean.student.AcademicInfo;
import com.bean.student.StudentDetails;
import com.dao.examination.EligibleCheckDetails;
import com.dao.examination.StudentMarkSheetData;
import com.dao.student.StudentData;

/***
 * This is a Marks Utility class used to Set marks,
 * Get marks , show ranks of different Exams
 * @author Batch-C
 *
 */
public class SetMarkSheetForAllExams {

	// creating a list for MpC marks
		static List<SetMarkSheet> MPClist = new ArrayList<SetMarkSheet>();
		
		/**
		 * This method checks whether student attended exam or not
		 * @param examChoice
		 * @throws FileNotFoundException
		 */
		public void setMarks(int examChoice) throws FileNotFoundException {
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			System.out.println("enter the student id");
			long stud_id = Long.parseLong(sc.nextLine());
			if (new SetMarkSheetForAllExams().checkEligible(stud_id)) {
				new SetMarkSheetForAllExams().SetStudentsMarksForAllExams(stud_id, examChoice);
			} else {
				System.out.println("Student is not eligible for exam.");
			}
		}
		/**
		 * This method checks whether student eligible for exam or not
		 * @param stud_id
		 * @return true if there is a studentList in the file
		 * @throws FileNotFoundException
		 */
		public boolean checkEligible(long stud_id) throws FileNotFoundException {
			List<AcademicInfo> list = new ArrayList<AcademicInfo>();
			list = EligibleCheckDetails.getAcademicList();
			if (list.size() == 0) {
				System.out.println("Student List is Empty");
			} else {
				Iterator<AcademicInfo> it = list.iterator();
				while (it.hasNext()) {
					AcademicInfo st1 = (AcademicInfo) it.next();
					if (stud_id == st1.getStudent_id()) {
						if (st1.getCheck().equalsIgnoreCase("yes")) {
							return true;
						}
					}
				}
			}
			return false;
		}
		/**
		 * This method set type of exam student should attend and as per 
		 * exam it set the file path to store mark sheet
		 * @param stud_id
		 * @param examChoice
		 * @throws FileNotFoundException
		 */
		public void SetStudentsMarksForAllExams(long stud_id, int examChoice) throws FileNotFoundException {
			
			String filePath = null;
			String[] studentInfo = null;
			try {
				studentInfo = getStudentNameBranch(stud_id);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			// setting text file path to store records in file
			if (examChoice == 1 && studentInfo[2].equalsIgnoreCase("mpc")) {
				filePath = "ExamTrackingDataFiles\\MPCmid1marks";
			} else if (examChoice == 1 && studentInfo[2].equalsIgnoreCase("bpc")) {
				filePath = "ExamTrackingDataFiles\\BPCmid1marks";
			} else if (examChoice == 2 && studentInfo[2].equalsIgnoreCase("mpc")) {
				filePath = "ExamTrackingDataFiles\\MPCmid2marks";
			} else if (examChoice == 2 && studentInfo[2].equalsIgnoreCase("bpc")) {
				filePath = "ExamTrackingDataFiles\\BPCmid2marks";
			} else if (examChoice == 3 && studentInfo[2].equalsIgnoreCase("mpc")) {
				filePath = "ExamTrackingDataFiles\\MPCFinalmarks";
			} else if (examChoice == 3 && studentInfo[2].equalsIgnoreCase("bpc")) {
				filePath = "ExamTrackingDataFiles\\BPCFinalmarks";
			}
			
			if(checkMarkList(stud_id,filePath)) {
				System.out.println("Student Marks Sheet Already created");
			} else {
				setMarkSheetForStudent(filePath , examChoice , stud_id);
			}
		}
		/**
		 * This method checks whether mark Sheet is created or not
		 * @param id
		 * @param filePath
		 * @return
		 * @throws FileNotFoundException
		 */
		public boolean checkMarkList(long id, String filePath) throws FileNotFoundException {
			List<SetMarkSheet> list = new ArrayList<SetMarkSheet>();
			list = StudentMarkSheetData.getAllStudentMarksList(filePath);
			Iterator<SetMarkSheet> it = list.iterator();
			while (it.hasNext()) {
				SetMarkSheet marks = (SetMarkSheet) it.next();
				if (id == marks.getStudent_id()) {
					return true;
				}
			}
			return false;
		}
		/**
		 * This after selecting exam this method generates marksheet
		 * @param filePath
		 * @param examChoice
		 * @param stud_id
		 */
		@SuppressWarnings("unchecked")
		public void setMarkSheetForStudent(String filePath,int examChoice, long stud_id) {
			@SuppressWarnings("rawtypes")
			List list = null;
			try {
				list = StudentMarkSheetData.getAllStudentMarksList(filePath);
			} catch (FileNotFoundException e2) {
				
				e2.printStackTrace();
			}
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			String[] studentInfo = null;
			try {
				studentInfo = getStudentNameBranch(stud_id);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			SetMarkSheet mpc = new SetMarkSheet();
			if (examChoice == 3) {
				mpc.setStudent_id(stud_id);
				if (studentInfo[2].equalsIgnoreCase("mpc")) {
					System.out.println("Enter Marks for Mathematics");
				} else {
					System.out.println("Enter Marks for Biology");
				}
				int math = sc.nextInt();
				while (math <= 0 || math > 75) {
						System.out.println("Enter Mark Between 0 to 75");
						math = sc.nextInt();
					}
				mpc.setMaths(math);

				System.out.println("Enter Marks for Physics");
				int physics = sc.nextInt();
				while (true) {
					if (physics <= 0 || physics > 75) {
						System.out.println("Enter Mark Between 0 to 75");
						physics = sc.nextInt();
					} else {
						break;
					}
				}
				mpc.setPhysics(physics);

				System.out.println("Enter Marks for Chemistry");
				int chemistry = sc.nextInt();
				while (true) {
					if (chemistry <= 0 || chemistry > 75) {
						System.out.println("Enter Mark Between 0 to 75");
						chemistry = sc.nextInt();
					} else {
						break;
					}
				}
				mpc.setChemistry(chemistry);

				System.out.println("Enter Marks for English");
				int english = sc.nextInt();
				while (true) {
					if (english <= 0 || english > 75) {
						System.out.println("Enter Mark Between 0 to 75");
						english = sc.nextInt();
					} else {
						break;
					}
				}
				mpc.setEnglish(english);

				System.out.println("Enter Marks for Sanskit");
				int sanskrit = sc.nextInt();
				while (true) {
					if (sanskrit <= 0 || sanskrit > 75) {
						System.out.println("Enter Mark Between 0 to 75");
						sanskrit = sc.nextInt();
					} else {
						break;
					}
				}
				mpc.setSanskrit(sanskrit);

				int total = math + chemistry + physics + english + sanskrit;
				double percentage = (total * 100 ) / 425;
				mpc.setTotalmarks(total);
				mpc.setPercentage(percentage);
				mpc.setOutOfMarks(425);
			} else {
				mpc.setStudent_id(stud_id);
				if (studentInfo[2].equalsIgnoreCase("mpc")) {
					System.out.println("Enter Marks for Mathematics");
				} else {
					System.out.println("Enter Marks for Biology");
				}
				int math = sc.nextInt();
				while (true) {
					if (math <= 0 || math > 25) {
						System.out.println("Enter Mark Between 0 to 25");
						math = sc.nextInt();
					} else {
						break;
					}
				}
				mpc.setMaths(math);

				System.out.println("Enter Marks for Physics");
				int physics = sc.nextInt();
				while (true) {
					if (physics <= 0 || physics > 25) {
						System.out.println("Enter Mark Between 0 to 25");
						physics = sc.nextInt();
					} else {
						break;
					}
				}
				mpc.setPhysics(physics);

				System.out.println("Enter Marks for Chemistry");
				int chemistry = sc.nextInt();
				while (true) {
					if (chemistry <= 0 || chemistry > 25) {
						System.out.println("Enter Mark Between 0 to 25");
						chemistry = sc.nextInt();
					} else {
						break;
					}
				}
				mpc.setChemistry(chemistry);

				System.out.println("Enter Marks for English");
				int english = sc.nextInt();
				while (true) {
					if (english <= 0 || english > 25) {
						System.out.println("Enter Mark Between 0 to 25");
						english = sc.nextInt();
					} else {
						break;
					}
				}
				mpc.setEnglish(english);

				System.out.println("Enter Marks for Sanskit");
				int sanskrit = sc.nextInt();
				while (true) {
					if (sanskrit <= 0 || sanskrit > 25) {
						System.out.println("Enter Mark Between 0 to 25");
						sanskrit = sc.nextInt();
					} else {
						break;
					}
				}
				mpc.setSanskrit(sanskrit);

				int total = (math + chemistry + physics + english + sanskrit);
				double percentage = (total * 100) / 125;
				mpc.setTotalmarks(total);
				mpc.setPercentage(percentage);
				mpc.setOutOfMarks(125);

			}
			// common values for both branches
			int markshitId = 1;
			mpc.setMarkSheet_id(markshitId);
			mpc.setFirstName(studentInfo[0]);
			mpc.setLastName(studentInfo[1]);
			mpc.setBranch(studentInfo[2]);

			list.add(mpc);
			try {
				StudentMarkSheetData.saveAllStudentMarksList(list, filePath);
				System.out.println("Marks saved Successfully.");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		/**
		 * This get Gets student basic information from master file
		 * @param id 
		 * @return First name , last name , branch
		 * @throws FileNotFoundException
		 */
		public static String[] getStudentNameBranch(long id) throws FileNotFoundException {
			@SuppressWarnings({ "rawtypes", "unchecked" })
			List<StudentDetails> list = new ArrayList();
			list = StudentData.getStudentList();
			String[] stdDetails = new String[3];
			if (list.size() == 0) {
				System.out.println("Student List is Empty");
			} else {
				Iterator<StudentDetails> it = list.iterator();
				while (it.hasNext()) {
					StudentDetails st1 = (StudentDetails) it.next();
					if (st1.getStudent_id() == id) {
						stdDetails[0] = st1.getFirstname();
						stdDetails[1] = st1.getLastname();
						stdDetails[2] = st1.getBranch();
					}
				}
			}
			return stdDetails;

		}
}
